
# Benefit
Benefit of this container is that python and nordvpn
run detached from your machine and environment.

## Origin - Description BTAC
https://github.com/dlesbre/bibtex-autocomplete
```
bibtex-autocomplete or btac is a simple script to autocomplete BibTeX bibliographies. It reads a BibTeX file and looks online for any additional data to add to each entry. It can quickly generate entries from minimal data (a lone title is often sufficient to generate a full entry). You can also use it to only add specific fields (like DOIs, or ISSN) to a manually curated bib file.

It is designed to be as simple to use as possible: just give it a bib file and let btac work its magic! It combines multiple sources and runs consistency and normalization checks on the added fields (check that URLs lead to a valid webpage, that DOIs exist at https://dx.doi.org/).
```

## Origin - Description NordVPN Container
https://support.nordvpn.com/hc/en-us/articles/20465811527057-How-to-build-the-NordVPN-Docker-image
```
Docker is a tool for easy deployment and management of applications in lightweight containers. Setting up NordVPN on a Docker container will secure your internet connection and protect your online privacy of other Docker containers as well.
```

# Pre-Condition NorVPN (optional)
1. Organize a nordvpn token via the steps on
[NordVPN Manual](https://support.nordvpn.com/hc/en-us/articles/20286980309265-How-to-use-a-token-with-NordVPN-on-Linux)

# Usage
1. Build the container (command below);
1. Copy the desired bibtex file to this folder;
1. Start the container with access to this folder (command below);
1. Optional - Start the NordVPN connection (command below);
1. Run BTAC from the container (command below);
1. Done.
1. Profit.

## Build container
```
docker build -f './Dockerfile' --tag btac:test .;
```

## Start container
```
docker run \
  --workdir /data/ -it \
  --hostname mycontainer \
  --cap-add=NET_ADMIN \
  --sysctl net.ipv6.conf.all.disable_ipv6=0 \
  --volume ${PWD}:/data btac:test;
```

## Optional - Start the NordVPN
```
nordvpn login --token <your token>
nordvpn connect
```

## Run BTAC
```
btac --version;
btac input.bib;
```
