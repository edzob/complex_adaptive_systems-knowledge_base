# Complex Adaptive Systems PhD Research Repository/Wiki

## Logo
Temp logo is from 2014 from blog [PHDcast – The future and the current state of the media industry](https://ashtonmedia.com.au/phdcast-future-current-state-media-industry/)
## Content

This repository and wiki is a Summary 
and overview on available knowledge 
on the topic of:
1. Complexity Science in contrast to reductionistic science
1. Complex Adaptive Systems in the context of Complexity Science
1. Organisation as Complex Adaptive Systems
1. (Organisational) behaviour of organisations
1. Resilience as specific organisational "behaviour"
1. Human behaviour as specific element of resilience
1. human as a social being
1. human as an emotional being.

With the assumptions that:
1. Enterprise Architecture (EA) and Enterprise Engineering (EE) are part of the social science domain
1. Organisational Resilience is part of risk management, 
1. where risk management aims to "optimize" business continuity,
1. where security is an organisation capability in the domain of risk management.
1. the goal of an organisation is to stay relevant for its stakeholders,
and this is the result of business continity (staying relevant),
and this is next to the role of risk management also the goal of EA, EE and Enterprise Governance.

This wiki and repository is initially created in in context of a PhD research on this topic.

The aim is not to create an overview of the complete body of knowledge.
For this the domain is to big and there are other sites providing this.

### Wiki
Summary of available concepts with overview of documentation will be part of the Wiki

### Repository
**The Repository will be mostly empty.** 
The Repository will contain BibTeX files of available and used literature

#### BibTeX
The BibTeX files will (probbaly) be managed with zotero or mendeley, 
but the files will be manually enrichted and normalized. 
And this will be the one true source of the BibTeX files. 
For everybody to extend on.

Files will be sorted per categorie of sources (e.g. enterprise-engineering.bib).

#### Papers
In the future the Repository could contain (backup of) existing papers, 
and stored via the Git filesystem since those are large blobs.

## Relation to existing/new research

The conceptual model used in the research 
of the EAAL MSc thesis and EAAL articles 
are based on content from this body of knowledge. 

