1. Gibson, J. J. (1977). A Theory of Affordances. In R. Shaw & J. Bransford (Eds.), Perceiving, Acting and Knowing: Toward an Ecological Psychology (pp. 127-143). Lawrence Erlbaum Associates.

1. Orlikowski, W. J. (1992). The Duality of Technology: Rethinking the Concept of Technology in Organizations. Organization Science, 3(3), 398-427.

1. DeSanctis, G., & Poole, M. S. (1994). Capturing the Complexity in Advanced Technology Use: Adaptive Structuration Theory. Organization Science, 5(2), 121-147.

1. Leonardi, P. M., & Barley, S. R. (2008). Materiality and Change: Challenges to Building Better Theory about Technology and Organizing. Information and Organization, 18(3), 159-176.

1. Markus, M. L., & Silver, M. S. (2008). A Foundation for the Study of IT Effects: A New Look at DeSanctis and Poole’s Concepts of Structural Features and Spirit. Journal of the Association for Information Systems, 9(10), 609-632.

1. Norman, D. A. (1990). The Design of Everyday Things. Doubleday.

1. Burton-Jones, A., & Straub, D. W. (2006). Reconceptualizing System Usage: An Approach and Empirical Test. Information Systems Research, 17(3), 228-246.

1. Zammuto, R. F., Griffith, T. L., Majchrzak, A., Dougherty, D. J., & Faraj, S. (2007). Information Technology and the Changing Fabric of Organization. Organization Science, 18(5), 749-762.

1. Leonardi, P. M. (2011). When Flexible Routines Meet Flexible Technologies: Affordance, Constraint, and the Imbrication of Human and Material Agencies. MIS Quarterly, 35(1), 147-167.

1. Seidel, S., Recker, J., & vom Brocke, J. (2013). Sensemaking and Sustainable Practicing: Functional Affordances of Information Systems in Green Transformations. MIS Quarterly, 37(4), 1275-1299.

1. Karahanna, E., Xu, S. X., Xu, Y., & Zhang, A. (2018). The Needs–Affordances–Features Perspective for the Use of Social Media. MIS Quarterly, 42(3), 737-756.

1. Steffen, J. H., Gaskin, J. E., Meservy, T. O., Jenkins, J. L., & Wolman, I. (2019). Framework of affordances for virtual reality and augmented reality. Journal of Management Information Systems, 36(3), 683-729.

1. Faik, I., Barrett, M., & Oborn, E. (2020). How Information Technology Matters in Societal Change: An Affordance-Based Institutional Perspective. MIS Quarterly, 44(3), 1359-1390.

1. Lehrer, C., Wieneke, A., Vom Brocke, J. A. N., Jung, R., & Seidel, S. (2018). How big data analytics enables service innovation: materiality, affordance, and the individualization of service. Journal of Management Information Systems, 35(2), 424-460.

1. Popper, K. R. (1996). The Open Society and its Enemies: Hegel and Marx. Routledge.

1. Huang, J., Henfridsson, O., Liu, M. J., & Newell, S. (2017). Growing on Steroids: Rapidly Scaling the User Base of Digital Ventures Through Digital Innovation. MIS Quarterly, 41(1), 301-314.

1. Burton-Jones, A., Recker, J., Indulska, M., Green, P., & Weber, R. (2017). Assessing Representation Theory with a Framework for Pursuing Success and Failure. MIS Quarterly, 41(4), 1307-1333.

1. Recker, J., Indulska, M., Green, P., Burton-Jones, A., & Weber, R. (2019). Information Systems as Representations: A Review of the Theory and Evidence. Journal of the Association for Information Systems, 20(6), 735-786.

1. Autio, E., Nambisan, S., Thomas, L. D., & Wright, M. (2018). Digital affordances, spatial affordances, and the genesis of entrepreneurial ecosystems. Strategic Entrepreneurship Journal, 12(1), 72-95.

1. Malhotra, A., Majchrzak, A., & Lyytinen, K. (2021). Socio-Technical Affordances for Large-Scale Collaborations: Introduction to a Virtual Special Issue. Organization Science, https://doi.org/10.1287/orsc.2021.1457.
