Adebambo, B., Brockman, P. and Yan, X. (2015) ‘Anticipating the 2007–2008 Financial Crisis: Who Knew What and When Did They Know It?’ Journal of Financial and Quantitative Analysis, 50(04), pp. 647–669. doi: 10.1017/S0022109015000381.
Ahuja, G. and Katila, R. (2004) ‘Where do resources come from? The role of idiosyncratic situations’, Strategic Management Journal, 25(89), pp. 887–907. doi: 10.1002/smj.401.
Ahuja, G. and Katila, R. (01-AUG-2004) ‘Where do resources come from? the role of idiosyncratic situations’, Strategic management journal.
Akca, K. and Ozturk, S.S. (2016) ‘The Effect of 2008 Crisis on the Volatility Spillovers among Six Major Markets’, International Review of Finance, 16(1), pp. 169–178. doi: 10.1111/irfi.12071.
Aleksić, A., Stefanović, M., Arsovski, S. and Tadić, D. (2013) ‘An assessment of organisational resilience potential in SMEs of the process industry, a fuzzy approach’, Journal of Loss Prevention in the Process Industries, 26(6), pp. 1238–1245. doi: 10.1016/j.jlp.2013.06.004.
Alvesson, M. and Sköldberg, K. (2012) Reflexive methodology: New vistas for qualitative research. 2nd edn. Los Angeles, Calif.: Sage.
Amann, B. and Jaussaud, J. (2012) ‘Family and non-family business resilience in an economic downturn’, Asia Pacific Business Review, 18(2), pp. 203–223. doi: 10.1080/13602381.2010.537057.
Anderson, S. (2013) 2000th Issue 40 Years in Review: A history of the past 40 years in financial crises. Available at: http://www.ifre.com/a-history-of-the-past-40-years-infinancial-crises/21102949.fullarticle (Accessed: 14 April 2016).
Arndt, H. (2008) Supply Chain Management: Optimierung logistischer Prozesse. 4th edn. Wiesbaden: Gabler Verlag / GWV Fachverlage GmbH Wiesbaden. Available at: http://dx.doi.org/10.1007/978-3-8349-9743-2.
Ates, A. and Bititci, U. (2011) ‘Change process: A key enabler for building resilient SMEs’, International Journal of Production Research, 49(18), pp. 5601–5618. doi: 10.1080/00207543.2011.563825.
Beck, T., Demirgüç-Kunt, A., Laeven, L. and Maksimovic, V. (2006) ‘The determinants of financing obstacles’, Journal of International Money and Finance, 25(6), pp. 932– 952. doi: 10.1016/j.jimonfin.2006.07.005.
Bendell, T. (2014) Building anti-fragile organisations: Risk, opportunity and governance in a turbulent world. Farnham, Surrey: Gower. Available at: http://search.ebscohost.com/ login.aspx?direct=true&scope=site&db=nlebk&db=nlabk&AN=710315. Bendell, T. (2014) ‘Stress to impress’, Significance, 11(5), p. 81. doi: 10.1111/j.1740- 9713.2014.00788.x.
Berg, B.L. and Lune, H. (2014) Qualitative research methods for the social sciences. 8th edn. (Pearson custom library). Harlow: Pearson.
Blinov, A., Zacharov, V. and Zacharov, I. (2012) ‘Distinctive characteristics of personnel motivation in a crisis situation’, Problems of Economic Transition : a Journal of Translations from Russian, 54(10), pp. 3–12.
BMEI, (2011) ‘International Conference on Business Management and Electronic Information’. Guangzhou, China.
Bourletidis, K. (2013) ‘The Strategic Management of Market Information to SMEs during Economic Crisis’, Procedia - Social and Behavioral Sciences, 73, pp. 598–606. doi: 10.1016/j.sbspro.2013.02.096.
Broughton, A. (23. Juni / 2011) SMEs in the crisis: Employment, industrial relations and local partnership. Available at: https://www.eurofound.europa.eu/observatories/ eurwork/comparative-information/smes-in-the-crisis-employment-industrial-relationsand-local-partnership (Accessed: 14 April 2016).
Coutu, D.L. (2002) ‘How resilience works’, Harvard Business Review, 80(5), p. 46-50, 52, 55 passim.
Crema, M., Verbano, C. and Venturini, K. (2014) ‘Linking strategy with open innovation and performance in SMEs’, Measuring Business Excellence, 18(2), pp. 14– 27. doi: 10.1108/MBE-07-2013-0042.
Creswell, J.W. (2013) Research design: Qualitative, quantitative, and mixed method approaches. Los Angeles, Calif.: Sage.
Crichton, M.T., Ramsay, C.G. and Kelly, T. (2009) ‘Enhancing Organisational Resilience Through Emergency Planning: Learnings from Cross-Sectoral Lessons’, Journal of Contingencies and Crisis Management, 17(1), pp. 24–37. doi: 10.1111/j.1468-5973.2009.00556.x.
Cristescu, A., Stanila, L. and Andreica, M.E. (oct) ‘Motivation of the public employee in Romania in the context of the economic crisis’, Theoretical and Applied Economics : GAER Review, XX.2013.
Cronbach, L.J. (1969, ©1970) Essentials of psychological testing. 3rd edn. New York: Harper & Row.
Dasari, S., Jigeesh, N. and Pradhukumar, A. (2015) ‘Analysis of project success issues: The case of a manufacturing SME’, The IUP Journal of Operations Management : IJOM, 14(1), pp. 32–38.
Dervitsiotis, K.N. (2010) ‘A framework for the assessment of an organisation's innovation excellence’, Total Quality Management & Business Excellence, 21(9), pp. 903–918. doi: 10.1080/14783363.2010.487702.
Dervitsiotis, K.N. (2010) ‘Developing full-spectrum innovation capability for survival and success in the global economy’, Total Quality Management & Business Excellence, 21(2), pp. 159–170. doi: 10.1080/14783360903549865.
Dervitsiotis, K.N. (2011) ‘The challenge of adaptation through innovation based on the quality of the innovation process’, Total Quality Management & Business Excellence, 22(5), pp. 553–566. doi: 10.1080/14783363.2011.568256.
Dervitsiotis, K.N. (2012) ‘An innovation-based approach for coping with increasing complexity in the global economy’, Total Quality Management & Business Excellence, 23(9-10), pp. 997–1011. doi: 10.1080/14783363.2012.728849.
Dibrell, C., Davis, P.S. and Craig, J. (2008) ‘Fueling Innovation through Information Technology in SMEs’, Journal of Small Business Management, 46(2), pp. 203–218. doi: 10.1111/j.1540-627X.2008.00240.x.
Dobbs, M. and Hamilton, R.T. (2007) ‘Small business growth: Recent evidence and new directions’, International Journal of Entrepreneurial Behavior & Research, 13(5), pp. 296–322. doi: 10.1108/13552550710780885.
Drenth, P.J.D. and Sijtsma, K. (2005) Testtheorie: Inleiding in de theorie van de psychologische test en zijn toepassingen. Houten: Bohn Stafleu van Loghum. Available at: http://dx.doi.org/10.1007/978-90-313-6540-1.
Easterby-Smith, M., Thorpe, R. and Jackson, P.R. (2008) Management research. 3rd edn. Los Angeles: Sage Publ.
Eisenhardt, K.M. and Graebner, M.E. (2007) ‘Theory building from cases: Opportunities and Challenges’, Academy of Management Journal, 50(1), pp. 25–32. doi: 10.5465/AMJ.2007.24160888.
Enander, A., Hede, S. and Lajksjö, Ö. (2015) ‘Why Worry? Motivation for Crisis Preparedness Work among Municipal Leaders in Sweden’, Journal of Contingencies and Crisis Management, 23(1), pp. 1–10. doi: 10.1111/1468-5973.12067.
European Commission (2003) Commission Recommendation concerning the definition of micro, small and medium-sized enterprises (C(2003) 1422). Available at: http://eurlex.europa.eu/legal-content/EN/LSU/?uri=CELEX:32003H0361 (Accessed: 19 May 2016).
Feindt, S., Jeffcoate, J. and Chappell, C. (2002), Small Business Economics, 19(1), pp. 51–62. doi: 10.1023/A:1016165825476.
Given, L.M. (ed.) (2008) The Sage encyclopedia of qualitative research methods. London: Sage. Available at: http://search.ebscohost.com/ login.aspx?direct=true&scope=site&db=nlebk&db=nlabk&AN=525887.
Glenn, M. (2009) Organisational agility: How business can survive and thrive in turbulent times.
Goldratt, E.M. and Cox, J. (2004) The goal: A process of ongoing improvement. 3rd edn. Great Barrington, Mass.: North River Press.
Gomezelj Omerzel, D. and Antončič, B. (2008) ‘Critical entrepreneur knowledge dimensions for the SME performance’, Industrial Management & Data Systems, 108(9), pp. 1182–1199. doi: 10.1108/02635570810914883.
Grant, R.M. and Jordan, J.J. (2015) Foundations of Strategy: Wiley. Available at: https://books.google.se/books?id=uCFiBwAAQBAJ.
Grant, R.M. (2013) Contemporary strategy analysis. 8th edn. Chichester: Wiley.
Gryskiewicz, S.S. (1999) Positive turbulence: Developing climates for creativity, innovation, and renewal. San Francisco, Calif.: Jossey-Bass.
Gryskiewicz, S.S. (2005) ‘Leading renewal: The value of positive turbulence’, Leadership in Action, 25. doi: 10.1002/lia.1100.
Gryskiewicz, S.S. (2005) ‘Leading renewal: The value of positive turbulence’, Leadership in Action, 25. doi: 10.1002/lia.1100.
International Conference on Business Management and Electronic Information (BMEI), 2011: 13 - 15 May 2011, Guangzhou, China ; proceedings ; [including] 2011 International Conference on Supernetworks and System Management (ICSSM 2011), May 29 to 30, 2011, Shanghai, China (2011). Piscataway, NJ: IEEE. Available at: http://ieeexplore.ieee.org/servlet/opac?punumber=5871804.
Han, Y.-c. ‘Innovation:The way to enhance the core competitiveness of SMEs in the post-financial crisis era’, 2011 International Conference on Business Management and Electronic Information (BMEI), Guangzhou, China, pp. 581–584. doi: 10.1109/ICBMEI.2011.5917003.
Herbane, B. (2010) ‘Small business research: Time for a crisis-based view’, International Small Business Journal, 28(1), pp. 43–64. doi: 10.1177/0266242609350804.
Hilmersson, M. (2014) ‘Small and medium-sized enterprise internationalisation strategy and performance in times of market turbulence’, International Small Business Journal, 32(4), pp. 386–400. doi: 10.1177/0266242613497744.
Hilmersson, M., Jansson, H. and Sandberg, S. (2011) ‘Chapter 4 Experiential Knowledge Profiles of Internationalising SMEs – The Ability to Sustain Market Positions in the New Turbulent Era of Global Business’, in Verbeke, A., Tavares, A.T. and van Tulder, R. (eds.) Entrepreneurship in the global firm. (Progress in International Business Research, v. 6). Bingley: Emerald, pp. 77–96.
Holtbrügge, D. (2015) Internationales management: Theorien, funktionen, fallstudien. 6th edn. Stuttgart, Germany: Schäffer-Poeschel Verlag. Available at: http://site.ebrary.com/lib/alltitles/docDetail.action?docID=11093147.
Home, J.F. and Orr, J.E. (1997) ‘Assessing behaviors that create resilient organisations’, Employment Relations Today, 24(4), pp. 29–39. doi: 10.1002/ert.3910240405.
Jacob Morgan (2016) What Does Leadership Look Like In The Future Of Work?, 28 March. Available at: http://www.forbes.com/sites/jacobmorgan/2016/03/28/whatdoes-leadership-look-like-in-the-future-of-work/?xing_share=news (Accessed: 5 April 2016).
Kelley, T.L. (1927) Interpretation of educational measurements. (Measurement and adjustment series). Yonkers-on-Hudson: World Book Co.
Kindleberger, C.P. (2002) Manias, panics and crashes: A history of financial crises. 4th edn. Basingstoke: Palgrave. Available at: http://www.palgraveconnect.com/pc/ doifinder/10.1057/9780230536753.
Kotler, P. and Caslione, J.A. (2009) Chaotics: The business of managing and marketing in the age of turbulence. New York: American Management Association. Available at: http://site.ebrary.com/lib/academiccompletetitles/home.action.
Kraus, S., Rigtering, J.P.C., Hughes, M. and Hosman, V. (2012) ‘Entrepreneurial orientation and the business performance of SMEs: A quantitative study from the Netherlands’, Review of Managerial Science, 6(2), pp. 161–182. doi: 10.1007/s11846- 011-0062-9.
Lengnick-Hall, C.A. and A. (2009) ‘Resilience Capacity and Strategic Agility: Prerequisites for Thriving in a Dynamic Environment’, Working Papers (0059).
Lengnick-Hall, C.A., Beck, T.E. and Lengnick-Hall, M.L. (2011) ‘Developing a capacity for organisational resilience through strategic human resource management’, Human Resource Management Review, 21(3), pp. 243–255. doi: 10.1016/j.hrmr.2010.07.001.
Levy, D. (1994) ‘Chaos theory and strategy: Theory, application, and managerial implications’, Strategic Management Journal, 15(S2), pp. 167–178. doi: 10.1002/smj.4250151011.
Lin, C.Y.-Y. (1998) ‘Success Factors of Small- and Medium-Sized Enterprises in Taiwan: An Analysis of Cases’, Journal of Small Business Management, 36(4), p. 43. Lin, F.-J. and Lin, Y.-H. (2016) ‘The effect of network relationship on the performance of SMEs’, Journal of Business Research, 69(5), pp. 1780–1784. doi: 10.1016/j.jbusres.2015.10.055.
Love, J.H. and Roper, S. (2015) ‘SME innovation, exporting and growth: A review of existing evidence’, International Small Business Journal, 33(1), pp. 28–48. doi: 10.1177/0266242614550190.
Madrid-Guijarro, A., Garcia, D. and van Auken, H. (2009) ‘Barriers to Innovation among Spanish Manufacturing SMEs’, Journal of Small Business Management, 47(4), pp. 465–488. doi: 10.1111/j.1540-627X.2009.00279.x.
Malik, F.F. (2014) Führen Leisten Leben: Wirksames Management für eine neue Welt. Frankfurt, New York: Campus Verlag GmbH. Available at: http://search.ebscohost.com/login.aspx?direct=true&scope=site&db=nlebk&AN=1052170.
Mamouni Limnios, E.A., Mazzarol, T., Ghadouani, A. and Schilizzi, S.G. (2014) ‘The Resilience Architecture Framework: Four organisational archetypes’, European Management Journal, 32(1), pp. 104–116. doi: 10.1016/j.emj.2012.11.007.
Mason, J. (2011) Qualitative researching. 2nd edn. Los Angeles: Sage Publ. Maura Sheehan, N.K. Saunders, M., E. Gray, D. and Goregaokar, H. (2013) ‘SME innovation and learning: The role of networks and crisis events’, European Journal of Training and Development, 38(1/2), pp. 136–149. doi: 10.1108/EJTD-07-2013-0073.
Maxwell, J.A. (2013) Qualitative research design: An interactive approach. 3rd edn. (Applied social research methods series, 41). Los Angeles, Cal.: Sage.
Mazzei, A. and Ravazzani, S. (2015) ‘Internal Crisis Communication Strategies to Protect Trust Relationships: A Study of Italian Companies’, International Journal of Business Communication, 52(3), pp. 319–337. doi: 10.1177/2329488414525447.
Mintzberg, H. (1996) The rise and fall of strategic planning. 5th edn. New York: Prentice Hall.
Naidoo, V. (2010) ‘Firm survival through a crisis: The influence of market orientation, marketing innovation and business strategy’, Industrial Marketing Management, 39(8), pp. 1311–1320. doi: 10.1016/j.indmarman.2010.02.005.
Pal, R., Torstensson, H. and Mattila, H. (2014) ‘Antecedents of organisational resilience in economic crises—an empirical study of Swedish textile and clothing SMEs’, International Journal of Production Economics, 147, pp. 410–428. doi: 10.1016/j.ijpe.2013.02.031.
Panagiotakopoulos, A. (2013) ‘The impact of employee learning on staff motivation in Greek small firms: The employees&apos; perspective’, Development and Learning in Organisations, 27(2), pp. 13–15. doi: 10.1108/14777281311302030.
Panagiotakopoulos, A. (2014) ‘Enhancing staff motivation in “tough” periods: Implications for business leaders’, Strategic Direction, 30(6), pp. 35–36. doi: 10.1108/SD-05-2014-0060.
Papaoikonomou, E., Segarra, P. and Li, X. (2012) ‘Entrepreneurship in the Context of Crisis: Identifying Barriers and Proposing Strategies’, International Advances in Economic Research, 18(1), pp. 111–119. doi: 10.1007/s11294-011-9330-3.
Peters, T.J. (1987) Thriving on chaos: Handbook for a management revolution. New York: Harper & Row.
Philip, M. (2010) ‘Factors affecting business success of small & medium enterprises (SMEs)’, Asia Pacific Journal of Research in Business Management, 1(2), pp. 1–15.
Phillips, F. and Su, Y.-S. (2013) ‘Chaos, Strategy, and action: How not to fiddle while Rome burns’, International Journal of Innovation and Technology Management, 10(06), p. 1340030. doi: 10.1142/S0219877013400300.
Porter, M.E. (1996) What is strategy? (Harvard Business Review). Boston, Mass.: Harvard Business School Press.
Power, B. (2013) ‘Make your organisation anti-fragile.’, Finweek, 7 November, pp. p.36–37.
Powley Edward H. and Powley Will (2012) ‘Building Strength and Resilience: How HR Leaders Enable Healing in Organisations’, People & Strategy (4), 2012, pp. 42–47.
Qayum, M., Sawal, S.H. and Khan, H.M. (2014) ‘Motivating employees through incentives: productive or a counterproductive strategy’, JPMA. the Journal of the Pakistan Medical Association, 64(5), pp. 567–570.
Ramukumba, T. (2014) ‘Overcoming SMEs Challenges through Critical Success Factors: A Case of SMEs in the Western Cape Province, South Africa’, Economic and Business Review, 16(1), pp. 19–38. Available at: http://www.ebrjournal.net/ojs/ index.php/ebr/article/download/338/pdf_5.
Rašković, M., Makovec Brenčič, M. and Mörec, B. ‘Trust and management-toemployee communication in Slovenian companies: Some evidence from the current economic crisis, Tržište.
Raźniak, P. and Winiarczyk-Raźniak, A. (2015) ‘Did the 2008 global economic crisis affect large firms in Europe?’ Acta geographica Slovenica, 55(1). doi: 10.3986/AGS.740.
Rockart, J.F. (1979) A new approach to defining the chief executive's information needs. 1978th edn. (Sloan WP, 1008-78). Cambridge, Mass.: Center for Information Systems Research, Sloan School of Management, Massachusetts Inst. of Technol.
Robson, C. (2011) Real world research: A resource for users of social research methodsin applied settings. 3rd edn. Chichester: Wiley.
Rosenberg, M.B. (2004) Practical spirituality: Reflections on the spiritual basis of Nonviolent Communication. Encinitas, Calif: PuddleDancer Press. Available at: http://site.ebrary.com/lib/academiccompletetitles/home.action.
Rudestam, K.E. and Newton, R.R. (2007) Surviving your dissertation: A comprehensive guide to content and process. 3rd edn. Los Angeles Calif. u.a.: Sage.
Rupeika-Apoga, R. and Danovi, A. (2015) ‘Availability of Alternative Financial Resources for SMEs as a Critical Part of the Entrepreneurial Eco-System: Latvia and Italy’, Procedia Economics and Finance, 33, pp. 200–210. doi: 10.1016/S2212-5671(15)01705-0.
Ryder, B. ‘Essay - Financial Crises: The slumps that shaped modern finance’, The Economist. Available at: http://www.economist.com/news/essays/21600451-financenot-merely-prone-crises-it-shaped-them-five-historical-crises-show-how-aspectstoday-s-fina (Accessed: 14 April 2016)
Saini, D.S. (2015) ‘Book Review: Tony Bendell, Building Anti-fragile Organisations-- Risk, Opportunity and Governance in a Turbulent World’, Vision: The Journal of Business Perspective, 19(2), pp. 179–180. doi: 10.1177/0972262915575664.
Sandberg, S. (2012) Internationalization processes of small and medium-sized enterprises: Entering and taking off from emerging markets. (Linnaeus University dissertations, 78). Kalmar: School of Business and Economics, Linnaeus University.
Schaeken, W. (ed.) (2000) Deductive reasoning and strategies. Mahwah, NJ: Erlbaum Associates. Available at: http://www.loc.gov/catdir/enhancements/fy0709/99043475-d.html.
Shane Thornton (2016) Business Resources Definition (Accessed: 5 April 2016).
Silverman, D. (2013) Doing qualitative research. 4th edn. Los Angeles, Calif.: Sage Publ.
Simón-Moya, V., Revuelto-Taboada, L. and Ribeiro-Soriano, D. (2015) ‘Influence of economic crisis on new SME survival: Reality or fiction?’ Entrepreneurship & Regional Development, 28(1-2), pp. 157–176. doi: 10.1080/08985626.2015.1118560.
Simpson, M., Padmore, J. and Newman, N. (2012) ‘Towards a new model of success and performance in SMEs’, International Journal of Entrepreneurial Behavior & Research, 18(3), pp. 264–285. doi: 10.1108/13552551211227675.
Smallbone, D., Deakins, D., Battisti, M. and Kitching, J. (2012) ‘Small business responses to a major economic downturn: Empirical perspectives from New Zealand and the United Kingdom’, International Small Business Journal, 30(7), pp. 754–777. doi: 10.1177/0266242612448077.
Soininen, J., Puumalainen, K., Sjögrén, H. and Syrjä, P. (2012) ‘The impact of global economic crisis on SMEs’, Management Research Review, 35(10), pp. 927–944. doi: 10.1108/01409171211272660.
Somers, S. (2009) ‘Measuring Resilience Potential: An Adaptive Strategy for Organisational Crisis Planning’, Journal of Contingencies and Crisis Management, 17(1), pp. 12–23. doi: 10.1111/j.1468-5973.2009.00558.x.
Sullivan-Taylor, B. and Branicki, L. (2011) ‘Creating resilient SMEs: Why one size might not fit all’, International Journal of Production Research, 49(18), pp. 5565– 5579. doi: 10.1080/00207543.2011.563837.
Sung, T.K. (2011) ‘Dynamics of CSFs for business innovation: Normal vs. Crisis economic conditions’, Technological Forecasting and Social Change, 78(8), pp. 1310–1318. doi: 10.1016/j.techfore.2011.04.002.
Taleb, N.N. (2012) Antifragile: Things that gain from disorder. New York, NY: Random House.
Theingi and Purchase, S. (2011) ‘How exporters’ and intermediaries’ resources influence export performance’, Australasian Marketing Journal (AMJ), 19(2), pp. 100–107. doi: 10.1016/j.ausmj.2011.03.002.
Thiétart, R.A. and Forgues, B. (1995) ‘Chaos Theory and Organisation’, Organisation Science, 6(1), pp. 19–31. doi: 10.1287/orsc.6.1.19.
Tseitlin, A. (2013) ‘The antifragile organisation’, Communications of the ACM, 56(8), pp. 40–44. doi: 10.1145/2492007.2492022.
Vargo, J. and Seville, E. (2011) ‘Crisis strategic planning for SMEs: Finding the silver lining’, International Journal of Production Research, 49(18), pp. 5619–5635. doi: 10.1080/00207543.2011.563902.
Verbeke, A., Tavares, A.T. and van Tulder, R. (eds.) (2011) Entrepreneurship in the global firm. Bingley: Emerald (Progress in International Business Research, v. 6).
Watson, K., Hogarth‐Scott, S. and Wilson, N. (1998) ‘Small business start‐ups: Success factors and support implications’, International Journal of Entrepreneurial Behavior & Research, 4(3), pp. 217–238. doi: 10.1108/13552559810235510.
Wymenga, P., Spanikoova, V., Derbyshire, J., et al. (2010/2011) Are EU SMEs Recovering from the Crisis? Annual Report on EU Small and Medium sized Enterprises. Rotterdam.
Xiang, D. and Worthington, A. (2015) ‘Finance-seeking behaviour and outcomes for small- and medium-sized enterprises’, International Journal of Managerial Finance, 11(4), pp. 513–530. doi: 10.1108/IJMF-01-2013-0005.
Yin, R.K. (2014) Case study research: Design and methods. 5th edn. Los Angeles CA u.a.: SAGE Publ.