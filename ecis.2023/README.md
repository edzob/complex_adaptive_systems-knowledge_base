# ECIS 2023 - paper
European conference of Information Systems.

- Paper publication is closed source.
- Conference demands for blind review and for no related research previously published.
- Authors keep ownership
- Authors are therefor free to publish paper under open science


[[_TOC_]]

# ECIS License
from https://ecis2023.no/submissions/call-for-papers/

```
For all papers accepted at ECIS 2023, authors retain the copyrights. However, by submitting a paper, authors agree that AIS can publish and reproduce any accepted papers in the ECIS 2023 proceedings in the format of AIS’ choosing (e-Library, printed proceedings, etc.). Authors are encouraged to develop their papers for journal publication after the conference. Both completed research papers (RP) and research in-progress papers (RiP) accepted and presented in interactive presentation sessions (or as posters) will be published in the ECIS conference proceedings and made available in the AIS Electronic Library.
```

# Gitlab CI pipeline for LaTeX example

This repository contains examples for the blog post 
[How to annoy your co-authors: a Gitlab CI pipeline for LaTeX](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/).

This latex-pipeline is using 
[Docker](https://docs.docker.com/get-docker/) 
and 
[GNU make](https://www.gnu.org/software/make/)
together with 
[latexMK](https://ctan.org/pkg/latexmk?lang=en) 
in a the 
[texlive:latest container](https://hub.docker.com/r/texlive/texlive).

The [texlive:latest container](https://hub.docker.com/r/texlive/texlive).
is updated weekly by the texlive organisation.


When you need to change the (advanced) setting, you can do this via the `Makefile` and `latexmkrc` files.

---

## Compile locally

```bash
     make clean render
```


##  Compile continiously the pdf when the input files are updated

```bash
     make clean render LATEXMK_OPTIONS_EXTRA=-pvc
```


## Refresh the PDF continuous

This runs the PDF viewer [Evince](https://wiki.gnome.org/Apps/Evince),
make sure you have it insalled.

```bash
     evince paper/latexmk/main.pdf
```

## Combine GitLab with Overleaf

* see [this wiki page](https://gitlab.com/edzob/antifragile-research/-/wikis/docs/Overleaf-and-Gitlab) on the Antifragile MSc wiki on how to configure Gitlab and Overleaf into the same project folder on your laptop.