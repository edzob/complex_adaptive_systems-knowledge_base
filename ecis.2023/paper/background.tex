\chapter{Theoretical Background and Hypotheses Development}


\section{Core Concepts}

Change is constant according to Heraclitus statement panta rhei (“everything flows”). 
How to deal with the rate of changes is for many years a topic in business science 
\citep{book-ries-2011, book-mintzberg-1994}.
\cite{book-optland-2008} and \cite{book-hoogervorst-2017} 
argue that the goal of an organisation is to stay relevant to its stakeholders (including employees).
Organisation are complex-adaptive-systems
\citep{book-jackson-2019,book-stacey-2007, article-dietz-2013,book-hoogervorst-2017} 
consisting of cooperating human beings with a certain social purpose 
\citep{article-dietz-2013,book-hoogervorst-2017}. 

From 2014 onward, the term VUCA became more common 
in the field of business management and business science. 
VUCA is an acronym for Volatility, Uncertainty, Complexity and Ambiguity
\citep{article-bennet-2014-bh,article-bennet-2014-bhr,book-mack-2015}.
The field of risk management (ISO 31000:2018) acknowledges that organisations operate in a VUCA world 
\citep{book-hutchins-2018,article-risk-aven-2011,aven2012foundational,article-risk-aven-2012,article-risk-aven-2015,aven2016risk,book-aven-2018,jensen2018new,aven2019fundamental}.

For organisations, as social entities, 
to create and deliver value to their stakeholders,
information systems play an important role
\citep{book-DeHaes-2020}, 
if not a crucial role
\citep{article-orlikowski-1992}.
It is the application of an information system that 
determines the value that specific system adds. 
This is captured by the viewpoint of affordance.
This research adopts the definition of technology affordance by 
\cite{article-faik-2020}
 `as possibilities for goal-oriented action that an actor or
group of actors perceive in a technology as they engage 
with the materiality of its artefacts.'

Organisations are confronted with high impact changes. 
”In a March 2014 poll conducted by Inc. magazine, 
28\% of the respondents’ business models changed in the last five years; 
36\% of the respondents' mix of products and services changed; 
and 47\% of the respondents’ financial structure changed”. 
\cite{article-anthony-2016} states that organisations that continually reinvent themselves 
play a role in the controlling of their own (organisational) destiny.
\cite{article-makridakis-2009-forecasting} provides a long list 
that argues that risk cannot be managed by models, 
underlining the relevance to deal with unpredictability.

Chaos or unpredictability can be organised into two classical fields: 
objective chaos and subjective chaos 
\citep{article-risk-Derbyshire-2014,article-botjes-2021-chaos}.
Objective chaos is the chaos created by 
the systems interacting with each other as described by 
\cite{article-lorenz-1963}. 
Objective chaos can be mitigated
\citep{article-botjes-2021,article-botjes-2021-chaos}
by applying decoupling strategies
\citep{book-mannaert-2016}
that reduce the variety of the system as a whole 
\citep{article-ashby-1958,book-beer-1979}.
Subjective chaos is caused by 
individualistic perception, experience and knowledge
\citep{book-jackson-2019,book-bennett-2022,book-taleb-2001, book-taleb-2007}. 
The significance of subjectivity or relativity 
is a common perspective in philosophy, 
as for example in the famous ‘The Allegory of the Cave’ by Plato 
\citep{book-mcaleer-2020}.
Kant, a more recent philosopher, refers to the difference between subjective and objective reality as: 
``\textit{das ding für mich} and \textit{das ding an sich}.``.

In \cite{article-botjes-2021} 
%and \cite{thesis-botjes-2020}
the learning organisation is viewed via the lenses of
\cite{book-senge-1990} and 
\cite{article-garvin-2008,article-garvin-1993}
and identified as important to antifragile behaviour.
\cite{msc-thesis-bliekendaal-2022}
hints to the learning organisation being a critical success factor 
in respect to reducing (or embracing) 
the subjective and objective chaos. 
The relevance of learning was also argued in 
\cite{book-hoogervorst-2017},
as part of the Enterprise Engineering Sigma-theory, 
as a logical reflection on Variety and Requisite Variety
\citep{book-ashby-1956,book-beer-1979},
that the freedom of human behaviour increases the internal variety 
and is the only way to have sufficient variety 
to deal with the chaotic (outside) world. 

This reasoning, based on the arguments of 
(1) subjective chaos and 
(2) internal variety, leads to the \textbf{hypothesis:} 
"The human factor is a critical success factor 
in enabling antifragile behaviour of an organisation". 