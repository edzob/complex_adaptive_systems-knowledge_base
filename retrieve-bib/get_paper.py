from semanticscholar import SemanticScholar #https://api.semanticscholar.org/api-docs/graph#tag/Paper-Data/operation/post_graph_get_papers
from unpywall.utils import UnpywallCredentials
from unpywall import Unpywall
import constants

UnpywallCredentials(constants.api_key_unpaywall)

def main():
  #this is the paper ID needed to find the references for
  getpaperId = '82be64ba16667992c4bba45e3e23944cf931634d'
  filename = getpaperId+".bib"
  sch = SemanticScholar(api_key=constants.api_key_s2)
  results = sch.get_paper(getpaperId) #; print(results)
  
  #reset file with article + referenced articels
  f = open(filename, "w")
  f.write("")
  f.close()

  #add the source paper
  f = open(filename, "a")
  f.write(create_bibtex(results,0))
  f.close()
  #apppend all references (only journal supported)
  f = open(filename, "a")
  i=0 #reference counter for debug
  for item in results.references:
      i = i+1
      f.write(create_bibtex(item,i))
  #done for reference

def create_bibtex (item,i):
  bibtex = ""
  
  if item.publicationTypes is None: type=""
  else: type = str(item.publicationTypes[0])    
  print("type["+str(i)+"]: ["+type+"]")
  
  try: journalname = item.journal['name']
  except: journalname = ""

  if type == 'JournalArticle' or type=='journal' or type=='Review' or journalname != "":              
    bibtex+="@article{ article-"+str(item.paperId)+",\n"
    bibtex+="  title     = {"+item.title+"}"+",\n"
  
    j=0 #distinguise 1st author from other author
    authors=""
    for author in item.authors:
      if j==0:
        j=j+1
        authors = author.name
      else:    
        authors += " and\n               "+author.name
    bibtex+="  author    = {"+authors
    bibtex+="}"+",\n"
    bibtex+="  year      = {"+str(item.year)+"}"+",\n"

    try: bibtex+="  month     = {"+str(item.publicationDate.month)+"}"+",\n"
    except: bibtex+="  month     = {"+"}"+",\n"; print("no month"); print(item)
    
    if  type == 'JournalArticle' or type=='journal' or type=='Review' or journalname != "":
      try: bibtex+="  journal   = {"+item.journal['name']+"}"+",\n"
      except: bibtex+="  journal   = {"+"}"+",\n"; print("no journal name\n"); print(item)
      
      try: bibtex+="  issn      = {"+item.publicationVenue['issn']+"}"+",\n"
      except: bibtex+="  issn      = {"+"}"+",\n"; print("no issn"); print(item)
      
      try: bibtex+="  volume    = {"+item.journal['volume']+"}"+",\n"
      except: bibtex+="  volume    = {"+"}"+",\n"; print("no volume"); print(item)
      
      try: bibtex+="  pages     = {"+item.journal['pages']+"}"+",\n"
      except: bibtex+="  pages     = {"+"}"+",\n"; print("no pages"); print(item)
    else: bibtex+="------- {"+type+"-------"+"}"+",\n"

    bibtex+="  publisher = {"+"}"+",\n"
    ###### ID's
    try: bibtex+="  DOI       = {"+item.externalIds['DOI']+"}"+",\n"
    except: bibtex+="  DOI       = {"+"}"+",\n"; print("no DOI"); print(item)
    
    bibtex+="  s2Id      = {"+str(item.paperId)+"}"+",\n"
    
    #MAG
    try: bibtex+="  MAG       = {"+item.externalIds['MAG']+"}"+",\n"
    except: bibtex+="  MAG       = {"+"}"+",\n"
    #CorpusId  
    try: bibtex+="  CorpusId  = {"+item.externalIds['CorpusId']+"}"+",\n"
    except: bibtex+="  CorpusId  = {"+"}"+",\n"
    #DBLP  
    try: bibtex+="  DBLP      = {"+item.externalIds['DBLP']+"}"+",\n"
    except: bibtex+="  DBLP      = {"+"}"+",\n"
    # ArXiv
    try: bibtex+="  ArXiv     = {"+item.externalIds['ArXiv']+"}"+",\n"
    except: bibtex+="  ArXiv     = {"+"}"+",\n"
    #ACL  
    try: bibtex+="  ACL       = {"+item.externalIds['ACL']+"}"+",\n"
    except: bibtex+="  ACL       = {"+"}"+",\n"
    # PubMed
    try: bibtex+="  PubMed    = {"+item.externalIds['PubMed']+"}"+",\n"
    except: bibtex+="  PubMed    = {"+"}"+",\n"
    #Medline
    try: bibtex+="  Medline   = {"+item.externalIds['Medline']+"}"+",\n"
    except: bibtex+="  Medline   = {"+"}"+",\n"
    # PubMedCentral
    try: bibtex+="  PubMedCentral = {"+item.externalIds['PubMedCentral']+"}"+",\n"
    except:bibtex+="  PubMedCentral = {"+"}"+",\n"

    try: bibtex+="  url       = {https://dx.doi.org/"+item.externalIds['DOI']+"}"+",\n"
    except: bibtex+=""
    try: bibtex+="  url       = {"+item.url+"}"+",\n"
    except: bibtex+=""
    try: bibtex+="  url       = {"+item.openAccessPdf['url']+"}"+",\n"
    except:  bibtex+=""

    bibtex+="}\n\n"
    print("bibtex entry complete, next\n") #for debug
    
  else: 
    bibtex+="{ "+str(i)+":"+str(item.paperId)+","
    bibtex+type
    bibtex+="}\n\n"
    print("no JournalArticle/ jounal type\n")   
    
  print(bibtex) #for debug
  print(item) #for debug
  return(bibtex) 
  
if __name__ == "__main__":
  main()