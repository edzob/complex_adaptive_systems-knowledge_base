%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\subsection{Construction of the conceptual model}
Both resilience and antifragility, 
in the context of organisational behaviour,
are describing the response of an organisational system
to an event, may it be internal or external.

The question this research wants to answer is, 
the significance of the learning organisation 
in improving in resilience.

To reason what plays a role in the behaviour of an 
organisation, a conceptual model is made.
The following reasoning is used 
in the construction of the conceptual model
used in this research.

\textbf{There is a relation between your mental model and the reality \citep[p.4]{cabrera2015unifying}.}
\begin{enumerate}
    \item Your mental models approximates reality.
    \item Reality provides input for your mental models. 

    \begin{figure}[H]
        \centering
        \includegraphics[width = 0.3\linewidth]{research.proposal/paper/images/mental_model_reality.png}
        \caption{Generic role of your mental models \citep{cabrera2015unifying}}
    \end{figure}
\end{enumerate}

\textbf{There is a relation between your mental model, your behaviour and results in reality.}\footnote{\url{https://nesslabs.com/mental-models}, \url{https://uxplanet.org/digital-design-mental-models-and-their-constant-state-of-change-8ad070f8f83d}}
\begin{enumerate}
    \setcounter{enumi}{2}
    \item Our mental models leads to behaviour.
    \item Behaviour leads to results via actions in reality.
    \item Results reinforce your mental model.
    \begin{figure}[H]
        \centering
        \includegraphics[width = 0.4\linewidth]{research.proposal/paper/images/mental_model_reality_behavior.png}
        \caption{Another generic role of your mental models}
    \end{figure}
\end{enumerate}

\textbf{There is a relation between your mental model, our shared conceptual model and the reality \citep{hestenes2006notes}.} See fig. \ref{fig:modelstructure}.

\begin{enumerate}
    \setcounter{enumi}{5}
    \item Your mental models play a role in creating a conceptual model.
    \item Our conceptual model plays a role in understanding our Mental model.
    \item Our conceptual model is a representation of Real things and real processes.
    \item Our conceptual model helps in the interpretation of real things and real processes.
    \item Your mental models play a role in the perception of reality.
    \item Your mental models play a role in the actions done in reality.
    
      \begin{figure}[H]
        \centering
        \includegraphics[width = 0.4\linewidth]{research.proposal/paper/images/hestenes-2006.png}
        \caption{Modeling Structure of Cognition by \cite{hestenes2006notes}}
        \label{fig:modelstructure}
      \end{figure}
\end{enumerate}

\textbf{Your perception of reality receives input via your senses.}\\ 
from \cite{book-callaghan-2019}
\begin{enumerate}
    \setcounter{enumi}{10}
    \item The input to your perception\footnote{\url{https://plato.stanford.edu/archives/fall2021/entries/perception-problem/}} 
    or observations by your mental model is done via your senses 
    \citep{book-callaghan-2019}.
    \item The known senses of a human are the classical five plus 
    two or more \citep{macpherson2011taxonomising}.
    Sense are also not discrete but influence each-other
    \citep{book-hamlyn-2022,privitera2018sensation}.
    \item Emotions play a role in Organizational Change 
    \citep{white2022leading}.
    
    \begin{enumerate}
        \item Vision,
        \item Hearing,
        \item Smelling,
        \item Feeling,
        \item Balance,
        \item Positioning (a.k.a. Proprioception, kinesthesia) 
            \citep{jones2000kinesthetic},
        \item Emotions 
            \citep{kryklywy2020architecture, schreuder2016emotional, klasen2012multisensory}.
        \item \ldots \\
    \end{enumerate}
\end{enumerate}

\textbf{We use symbols to construct conceptual models.}\\ 
from \cite{book-hestenes-2010,article-dietz-2017}
See fig. \ref{fig:triangle}.

\begin{enumerate}
    \setcounter{enumi}{13}
    \item Your mental models are a conceptualisation 
            of reality.
    \item Reality is a concretisation 
            of our mental models.
    \item Our conceptual model is an (shared) expression 
            of our mental modela.
    \item Our conceptual model is an interpretation 
            of your mental modela.
      \begin{figure}[H]
        \centering
        \includegraphics[width = 0.5\linewidth]{research.proposal/paper/images/hestenes-2010-and-hoogervorst-2017.png}
        \caption{Intuition of structure by \cite{book-hestenes-2010} and the model triangle by \cite{article-dietz-2017}}
        \label{fig:triangle}
      \end{figure}

\end{enumerate}

\textbf{Your mental model consists of layers.}\\ 
from \cite{article-korthagen-2004,book-ditls-1990} See fig. \ref{fig:onion}.

\begin{enumerate}
    \setcounter{enumi}{17}
    \item Your behaviour into your environment as part of reality 
        is influenced by many internal layers.
    
    \begin{enumerate}
        \item Skills and Capabilities (Competencies),
        \item Values and Beliefs,
        \item Identity and
        \item Mission / Spirituality\\
    \end{enumerate}
    
    \begin{figure}[H]
        \centering
        \includegraphics[width = 0.3\linewidth]{research.proposal/paper/images/korthagen.png}
        \includegraphics[width = 0.3\linewidth]{research.proposal/paper/images/dilts.png}
        \caption{Onion Model and Neurological Levels Model by \cite{article-korthagen-2004,book-ditls-1990}}
        \label{fig:onion}
    \end{figure}

\end{enumerate}

\textbf{Your behaviour is the result of your capabilities and convictions.}\\ 
from \cite{book-fishbein-2011} See fig. \ref{fig:raa}.

\begin{enumerate}
    \setcounter{enumi}{18}
    \item Your behaviour into reality 
    is the result of the following internal layers influencing each-other.
    \begin{enumerate}
        \item Your background factors,
        \item your belief and attitude towards behaviour,
        \item your belief and attitude towards Norms,
        \item your belief and attitude to have control,
        \item your intention and
        \item your capabilities. \\
    \end{enumerate}
    \begin{figure}[H]
        \centering
        \includegraphics[width = 0.8\linewidth]{research.proposal/paper/images/RAA.png}
        \caption{The Reasoned Action Approach model by and from \cite{book-fishbein-2011}}
        \label{fig:raa}
    \end{figure}
 \end{enumerate}

\textbf{Behaviour affects and guides behaviour.}\\
from \cite{book-archer-1995, archer2013morphogenic, book-hoogervorst-2017}. 
See fig. \ref{fig:archer}.

\begin{enumerate}
    \setcounter{enumi}{19}   
    \item Organisational behaviour 
        is the results of Structures and systems,
        human behaviour and culture.
    \item Human behaviour affects human behaviour.
    \item Employee behaviour affects management behaviour.
    \item Management behaviour guides employee behaviour.
    \item Human behaviour is only affected by human behaviour
    \item Human behaviour is guided by structure and systems and culture.
    \begin{figure}[H]
        \centering
        \includegraphics[width = 1.0\linewidth]{research.proposal/paper/images/archer-by-hoogervorst.png}
        \caption{ Morphogenic social system model 
        and Morphogenic enterprise system model 
        from \cite{book-hoogervorst-2017} 
        inspired by \cite{book-archer-1995, archer2013morphogenic}}
        \label{fig:archer}
    \end{figure}
\end{enumerate}

\newpage
\subsection{Research lenses}
I acknowledge the replication crisis 
in the field of psychology \citep{stuk-rood-vlees-7, YANSS-100, OSFParti94,Egodeple49:online, article-hagger-2016, article-Ioannidis-2005, article-open-reproducibility-2015}
and in the field of economics \citep{book-hodgson-1991,article-helbing-2010}.
In \cite{thesis-botjes-2020,article-botjes-2021-chaos}
I come to the same conclusion as \cite{article-kaleliouglu-2021} 
that the replication problem applies 
to all social sciences.
I reason that the replication crisis is the result
of the combination of 
objective chaos (chaos theory) 
and subjective chaos (perspective) 
\citep{thesis-botjes-2020,article-botjes-2021-chaos, book-taleb-2001,book-taleb-2007, book-jackson-2019,book-bennett-2022}.

Designing and managing the organisation to stay relevant is the shared goal of the expertise of 
Risk Management, 
Enterprise Architecture (EA), 
Enterprise Engineering (EE) and 
Enterprise Governance (EG).
EG, EE and EA 
are part of the social science domain.

The goal of an organisation 
is to stay relevant for its stakeholders
\citep{book-optland-2008, book-hoogervorst-2017}.
Business continuity is about staying relevant (ref).
Risk management aims to "optimise" 
business continuity 
\citep{book-hutchins-2018}.
Organisational Resilience is part of risk management
(ref needed).
Cyber-Security and Information Security Management 
are organisational capability 
in the domain of risk management 
(ref needed).
Therefore information Security Management 
is a social science.


\newpage
\subsection{Research Infrastructure}

All my research will be done in public GitLab repositories.
The research notes are versioned in a wiki
    {\footnotesize \url{https://gitlab.com/edzob/complex_adaptive_systems-knowledge_base/-/wikis/home}}. 
    The research project files are versioned in a repository 
    {\footnotesize \url{https://gitlab.com/edzob/complex_adaptive_systems-knowledge_base/-/tree/master/}}.
    This paper is versioned on GitLab 
    {\footnotesize \url{https://gitlab.com/edzob/complex_adaptive_systems-knowledge_base/-/tree/master/research.proposal}} and is versioned on Overleaf {\footnotesize \url{https://www.overleaf.com/read/wncjywdqdhpc}}.
