

\subsection{Theoretical Analysis research topic}
\textbf{Organisations need to stay relevant.}\\
Change is constant according to 
Heraclitus\footnote{\url{https://plato.stanford.edu/entries/process-philosophy/}}
statement \textit{panta rhei} (“everything flows”).
The how to deal with the rate of changes\footnote{\url{https://en.wikipedia.org/wiki/Rate_(mathematics)}} 
is for many years a topic in business science 
\citep{book-ries-2011, book-mintzberg-1994}.
\cite{book-optland-2008} and \cite{book-hoogervorst-2017} 
argue that the goal of an organisation is 
to stay relevant to its stakeholders (including employees).

\textbf{Organisations consists of people.}\\
Organisation are complex-adaptive-systems
\citep{book-jackson-2019,book-stacey-2007, article-dietz-2013,book-hoogervorst-2017} 
consisting of cooperating human beings with a certain social purpose 
\citep{article-dietz-2013,book-hoogervorst-2017}.

\textbf{Relevance demands change.}\\
Organisations are confronted with high impact changes.
"In a March 2014 poll conducted by Inc. magazine,
28\% of the respondents’ business models changed in the last five years; 
36\% of the respondents’ mix of products and services changed; and 
47\% of the respondents’ financial structure changed" (find ref\footnote{\url{https://www.excellius.com/blog/2019/11/21/frequency-of-change/}})

From 2014 onward the term VUCA became more common in 
the field of business management and business science 
(ref needed\footnote{on occurrence of term}).
VUCA is an acronym for Volatility, Uncertainty, Complexity and Ambiguity
\citep{article-bennet-2014-bh,article-bennet-2014-bhr,book-mack-2015}.
The field of risk management 
(ISO 31000:2018)
acknowledges that organisation
need to operate in a VUCA world 
\citep{book-hutchins-2018,article-risk-aven-2011,aven2012foundational,article-risk-aven-2012,article-risk-aven-2015,aven2016risk,book-aven-2018,article-jensen-2018,aven2019fundamental,book-aven-2019}.

\textbf{Certainty by change.}\\
\cite{article-anthony-2016} states that organisations that continually 
reinvent themselves
plays a role in the controlling of their own (organisational) destiny. 
\cite{article-makridakis-2009-forecasting} provides a long list
that argues that risk cannot be managed by models,
underlining the relevance to deal with unpredictability.


\begin{quote}
    ``We live in a ... VUCA time. ... 
    In terms of 
    ISO 31000:2018\footnote{\url{https://en.wikipedia.org/wiki/ISO_31000}} 
    and 
    ISO 9001:2015\footnote{\url{https://en.wikipedia.org/wiki/ISO_9000}}, 
    the concept of 'uncertainty' is integrated 
    throughout the standards.`` 
    \\- \citep[chapter 1]{book-hutchins-2018}
\end{quote}

\begin{quote}
    ``After all, we can’t know for sure what the future holds. 
    But companies can adapt to change 
    by launching new growth ventures 
    that move them beyond their historic core business. 
    Our work shows that the companies that 
    continually find ways to reinvent themselves 
    are the ones that control their own destiny.``
    \\- \citep[p. 8]{article-anthony-2016}.
\end{quote}

\textbf{Uncertainty is caused by connections.}\\
Edward Norton Lorenz\footnote{\url{https://en.wikipedia.org/wiki/Edward_Norton_Lorenz}} 
argues with a mathematical model that interconnections between systems are
causing the unpredictable behaviour of that systems \citep{article-lorenz-1963}.
In the field of chaos theory\footnote{\url{https://en.wikipedia.org/wiki/Chaos_theory}}
this is known as the 
butterfly-effect\footnote{\url{https://en.wikipedia.org/wiki/Butterfly_effect}}.
Complexity science studies non-linear behaviour 
\citep{book-jackson-2019}.

\begin{quote}
    [Chaos is \ldots] ``When the present determines the future, 
    but the approximate present 
    does not approximately determine 
    the future.`` - Edward Lorentz\footnote{\url{https://fricke.co.uk/Teaching/CS523_2017spring/Lectures/Lecture3_Dynamical_Systems.pdf}}
\end{quote}


In our (global) world the
number of connection in regard to 
transport of service, goods, money, labour, information and knowledge
keeps on increasing (ref needed from chaos paper).
COVID-19 boosted the global digital transformation resulting
in an increase of organisations embedding 
and providing digital services (add ref).

\textbf{Resilient to uncertainty.}\\
The absorption and recovery from change is called resilience 
\citep{article-botjes-2021,article-martin-breen-2011}.
The speed increasing frequency of change
asks for the optimisation of resilience.
Antifragile is as term introduced in \cite{book-taleb-2012}.
Antifragile is described as the anti-these of fragile.
Antifragile is a function of value over stress, where stress
is described as change. 
Antifragile can be used to optimise for resilience,
as it can be considered as the integral\footnote{\url{https://en.wikipedia.org/wiki/Integral}}
function ($f(stress)=value$) of resilience ($f(time)=value$) . 

\textbf{The relativity of uncertainty.}\\
Chaos or unpredictably 
can be organised into two classical fields:
objective chaos and subjective chaos \citep{article-risk-Derbyshire-2014,article-botjes-2021-chaos}.
Objective chaos is the chaos created by the systems interacting with each other as described by \cite{article-lorenz-1963}. 
Objective chaos can be mitigated \citep{article-botjes-2021-chaos,thesis-botjes-2020, article-botjes-2021}
by applying decoupling strategies \citep{book-mannaert-2016}
that reduce the variety of the system as a whole \citep{article-ashby-1958, book-beer-1979}.

Subjective chaos is caused by individualistic perception, 
experience and knowledge 
\citep{book-jackson-2019,book-bennett-2022, book-taleb-2001, book-taleb-2007}. 
The significance of subjectivity or relativity is 
a common perspective in philosophy, 
as for example in the famous \textit{The Allegory of the Cave}\footnote{\url{https://en.wikipedia.org/wiki/Allegory_of_the_cave}} 
by Plato \citep[p. 222]{book-mcaleer-2020}.
Kant\footnote{\url{https://plato.stanford.edu/entries/kant-transcendental-idealism/}}, 
a more recent and also famous philosopher,
referrers to the difference between subjective and objective reality as: 
\textit{das ding für mich} and \textit{das ding an sich}.  ``

\begin{quote}
    ``His [Kant] references meant that our subjective impressions (or perceptions) 
    of something may differ from its objective characteristics 
    \ldots Für mich refers to subjective perceptions and an sich refers to reality.`` 
    - \cite{article-mcneill-2014}
\end{quote}


\textbf{The role of the learning organisation}\\
In \cite{article-botjes-2021} and \cite{thesis-botjes-2020}
the learning organisation is viewed via the lenses of
\cite{book-senge-1990} and \cite{article-garvin-2008,article-garvin-1993}.

New research \citep{msc-thesis-bliekendaal-2022}
hints to the learning organisation being a critical success factor
in respect to reducing (or embracing)
the subjective and objective chaos.

It was also argued in \cite{book-hoogervorst-2017},
as part of the Enterprise Engineering Sigma-theory, 
as a logical reflection on Variety and Requisite Variety 
\citep{book-ashby-1956,book-beer-1979},
that the freedom of human behaviour 
increases the internal variety
and is the only way to deal with the chaotic (outside) world. 

In addition to the arguments of 
(1) subjective chaos and 
(2) internal variety,
the viewpoint of 
affordance (cite needed) 
adds to the relativity of the 
attributes defined in the 
Extendid Antifragile Attribute List (EAAL)
\citep{article-botjes-2021}.
The human factor plays a pivotal 
role for an organisation becoming 
antifragile.






\textbf{Information Security as research scope}\\
Information Security is a sub-domain 
of the risk management domain
\citep{article-Culot-2021-27001,book-hutchins-2018}.
The exposure to incident in the Information security domain 
is ever increasing
\citep{article-Culot-2021-27001}.
Information security recognises the importance of
the human factor in the response to incidents 
\citep{article-ali-2021}.
Information security recognises the importance
of absorbing change (conformance) 
as the enabler of creating value (performance)
\citep{book-hutchins-2018,article-huygh-2022}.
The cost of Information security incidents 
and the information security investments keep growing 
\citep{article-Yaqoob-2019}.

\begin{quote}
    ``[Gartner aug 2018:] Worldwide spending on information security products and services 
    will reach more than \$114 billion in 2018, 
    an increase of 12.4 percent from last year, 
    according to the latest forecast from Gartner, Inc. 
    In 2019, the market is forecast to grow 8.7 percent to \$124 billion.`` 
    - \cite{Informat32:online} 
\end{quote}

\begin{quote}
    ``[Gartner may 2021:] Worldwide spending on information security 
      and risk management technology and services 
      is forecast to grow 12.4\% to reach \$150.4 billion in 2021, 
      according to the latest forecast from Gartner, Inc. 
      Security and risk management spending grew 6.4\% in 2020.`` 
    - \cite{GartnerF92:online}
\end{quote}

\begin{quote}
    ``[Gartner dec 2021:] The stakes are also getting higher. 
       Gartner estimates by 2025, 
       40\% of boards of directors will have a dedicated cybersecurity committee 
       overseen by a qualified board member, up from less than 10\% today.`` 
    - \cite{Cybersec62:online} 
\end{quote}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\newpage
\subsection{Research Constructs}

\begin{enumerate}
    \item \textbf{Ontology}\\
    \textit{The study of concepts such as existence, being, becoming, and reality. It includes the questions of how entities are grouped into basic categories and which of these entities exist on the most fundamental level.\footnote{\url{https://en.wikipedia.org/wiki/Ontology}}.}
    
    \begin{enumerate}    
        \item Complexity Science
        \begin{enumerate}
            \item Complex-Adaptive-Systems by 
                \cite{book-jackson-2019}.
            \item Variety definition by 
                \cite{book-ashby-1956} and 
                \cite{book-beer-1979}.
            \item Viable Systems Model by
                \cite{book-beer-1979}.
        \end{enumerate}
        
        \item Personal Behaviour
        \begin{enumerate}
            \item Personal Behaviour via 
             Theory of Reasoned Action (TRA) by  
                \cite{book-fishbein-2011},
            Theory of Planned Behaviour (TPB) by    
                \cite{ajzen1991theory},
            The onion model by 
                \cite{article-korthagen-2004,article-korthagen-2017}
                and
                The Neurological Levels Pyramid by 
                \cite{book-ditls-1990, book-dilts-1995}.
            \item Philosophical view on neuroscience by 
                \cite{book-bennett-2022}.
        \end{enumerate}
    
        \item Organisational Behaviour
        \begin{enumerate}
            \item (Morphogenetic) Organisational Behaviour by 
                \cite{book-archer-1995,article-archer-2013-social,archer2013morphogenic,book-hoogervorst-2017, thesis-meriton-2016}.
            \item Function and Construction by 
                \cite{book-dietz-2020}.
            \item Affordance theory by .. to be cited
            \item Learning Organisation defined by 
                \cite{book-senge-1990}  and 
                \cite{article-garvin-2008,article-garvin-1993}.
            \item Enterprise Governance of IT by
                \cite{book-DeHaes-2020}.
        \end{enumerate}
    
        \item Antifragility/ Resilience
        \begin{enumerate}
            \item Risk Management by 
                \cite{book-hutchins-2018}.
            \item Resilience by 
                \cite{article-martin-breen-2011}.
            \item Antifragility by 
                \cite{book-taleb-2012}.
        \end{enumerate}
    \end{enumerate}

    \item \textbf{Epistemology}\\
    \textit{The study of the nature, origin, and scope of knowledge, epistemic justification, the rationality of belief, and various related issues\footnote{\url{https://en.wikipedia.org/wiki/Epistemology}}. }
    \begin{enumerate}    
        \item Philosophical view on reality by
            \cite{book-mcgilchrist-2021} 
            and 
            \cite{book-taleb-2001, book-taleb-2007}.
        \item The role of models in representing reality by         
            \cite{book-hestenes-2010,hestenes2006notes}.
        \item Chaos definition by Lorentz 
            \cite{wiki:Chaos_theory}.
        \item Philosophical view on neuroscience by 
            \cite{book-bennett-2022}.
        \item post-Modernism, post-Positivism, post-Empiricism, post-Structuralism and Deconstructionism) by
            ref needed
    \end{enumerate}
    
    
    \item \textbf{Methodology}\\
    \textit{The study of research methods\footnote{\url{https://en.wikipedia.org/wiki/Methodology}}.}
    



    \begin{enumerate}    
        \item Research approach and research findings adhere to 
        Open Science\footnote{\url{https://en.wikipedia.org/wiki/Open_science}}.
        
        \item Research will be licensed under the Creative Commons BY-SA 4.0\footnote{\url{https://creativecommons.org/licenses/by-sa/4.0/}}.
        \item Design Science will function as research framework, supporting mixed methods.
        
        \item The research life-cycle will follow 
        the Open Science Framework 
        (OSF\footnote{\url{https://www.cos.io/products/osf}}).
        
        \item During the research life-cycle the research (data) 
        will adhere to 
            Findable, Accessible, 
            Interoperable, and Reusable 
            (FAIR\footnote{\url{https://www.go-fair.org/fair-principles/}}).


        \item The research recognises 
            the quality attributes 
            Replicabiltiy, Falsification, 
            Independence and Precision by
            \cite{article-veronesi-2014,book-recker-2013} 
            and Karl Popper\footnote{\url{https://plato.stanford.edu/entries/popper}}.
            
        \item The research design recognises 
            the replication crisis in social science by 
            \cite{article-Ioannidis-2005, article-open-reproducibility-2015,article-hagger-2016,article-kaleliouglu-2021}.
        
        \item For the research design, 
            I need to look into action research 
            in combination with the postmodernism epistemological view 
            as written by    
            \cite{article-sousa-2010, thesis-rutzou-2015}.
        
        \item For the research design 
            I need to look into 
            research methodology in the epistemological view 
            critical realism 
            as written by
            \cite{article-mingers-2013,volkoff2013critical,galanter1962direct}.
            
        \item The research design will most likely focus on 
            qualitative research methods combined with
            triangulation for verification.
        
    \end{enumerate}        

    \item \textbf{Etiology}\\
        \textit{The study of the causes, origins, or reasons behind the way that things are, or the way they function, or it can refer to the causes themselves\footnote{\url{https://en.wikipedia.org/wiki/Etiology}}.}
    
    \begin{enumerate}
        \item \ldots
    \end{enumerate}
    
    \item \textbf{Teleology}\\
        \textit{The study of the destiny or purpose, as when we say something was "meant to be" or "happened for a reason"\footnote{\url{https://en.wikipedia.org/wiki/Teleology}}.}
    \begin{enumerate}
        \item \ldots
    \end{enumerate}

    \item Todo
    \begin{enumerate}
        \item Look into theory of agents en complex systems (new insights of Bruna Latour)
        \item Look into the relevance 
            of "family constellations" and "organisational constellations"
    \end{enumerate}

\end{enumerate}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \newpage
\subsection{Constructs relevant to Conceptual model}
\begin{enumerate}
    \item Complexity Science 
            in contrast to reductionist science.
    \item Complex Adaptive Systems 
            in the context of complexity science.
    \item Organisation 
        as complex adaptive systems.
    \item Organisational behaviour 
        as result of human behaviour, culture and structures.
    \item Resilience 
        as a specific organisational "behaviour".
    \item Antifragile
        as the optimum (differential) of resilience. 
    \item Human behaviour as specific element of resilience.
    \item Human as a social being.
    \item Human as an emotional being.
    \item Reality is objective (realistic view) 
            \textbf{AND }
          reality is subjective (subjective view) (archer and latour)
    \begin{enumerate}
    
        \item It is the subjective view on reality 
        that makes replication
        where the human behaviour plays a role impossible 
        (postmodernism, post-structuralism, post-positivism world view).
        
        \item Reality is real 
        since reality influences you as a human being.
        
        \item Critical Reality is the philosophical view that accepts
        subjective and objective both being true.
    \end{enumerate}
        
    \item Organisational learning is dependent on 5 disciplines 
    \citep{book-senge-1990}.

    \begin{enumerate}
        
        \item The theory of morphogensis, 
        supports the role of Systems Thinking 
        in organisational learning 
        \citep{archer2013morphogenic}.
        
        \item The theory of learning, 
        supports the concept of Systems Thinking 
        in learning
        \citep{article-korthagen-2017}.

        \item The theory of Reasoned Action Approach, 
        supports the concept of Systems Thinking 
        in behaviour
        \citep{book-fishbein-2011}.

    \end{enumerate}
    
\end{enumerate}

\newpage
\subsection{Hypothesis}

\begin{theorem}
    The human factor plays a significant role 
    as critical success factor in optimising the resilience of an organisation.
\end{theorem}

There are signs that support the hypothesis that the antifragile 
attributes (EAAL) create no value for the organisation
when the learning organisation can not support the definition,
realisation and implementation of these attributes.
Since the desired behaviour via the attributes need to fit the context, the function and the construction.

It can even be reasoned that to gain from chaos,
it is not antifragile behaviour that is needed, 
but that antifragile behaviour it the result of continuous learning. 

I want to research if I can proof this logical reasoning.
If the reasoning is true 
then the impact on how to solve problems 
(e.g. enterprise change governance) 
within organisations, need to be overhauled.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Expectation}