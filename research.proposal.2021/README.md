# Open University NL
[[_TOC_]]

# Template Settings
## Font - OU Branding.
* see [the wiki page][ou-wiki] of this research project on OU links
[ou-wiki](https://gitlab.com/edzob/complex_adaptive_systems-knowledge_base/-/wikis/Open-University)

### Brand
1. [Huisstijl](https://mijn.ou.nl/group/mdw/huisstijl)
1. [design manual pdf](https://mijn.ou.nl/documents/292081/4848957/Design_manual_5-03-2020_web.pdf/cf0e36fe-5a23-6026-9e3e-a9e58b866dda?t=1613055348389)
1. [Logo](https://mijn.ou.nl/group/mdw/-/logo)
1. [pptx](https://mijn.ou.nl/group/mdw/-/powerpoint-presentaties)
1. [schrijfwijzer pdf](https://mijn.ou.nl/documents/292081/4848957/Schrijfwijzer_afloop_definitief.pdf/f4144c47-c542-2bcb-4e88-dad42f0ecbac?t=1639039700183)
1. [Onderzoek - dataopslag](https://mijn.ou.nl/group/mdw/-/onderzoek-opslag-data)

* OU font for headings is [ANTIPASTO BOLD](https://www.1001fonts.com/antipasto-font.html) for H1 and ANTIPASTO DEMIBOLD for H2 and less.
* OU font for text is Myriad Pro 
* Google Font alternative is PT Sans
     * Only if the base font of the document is to be sans serif

```latex 
\usepackage{paratype}
\renewcommand*\familydefault{\sfdefault} 
\usepackage[T1]{fontenc}
```

## Font - Default for OU forms is Times New Roman
* Default Font in OU Forms is Times New Roman (and somethimes Arial), see [Learnui][learnui] for alternative fonts.
* Times New Roman is not a free font.
* For latex it is good to use a font that supports Math symbols and unicide.

### Alternative Google Font to Times New Roman
* The google font familiy is well supported, new and free to use.
* the Noto font is part of google font familiy and has extensive math and unicode support
* [Noto@google_font](https://fonts.google.com/noto/fonts)
* [Notomath@utwente](https://ftp.snt.utwente.nl/pub/software/tex/fonts/notomath/doc/notomath-doc.pdf)


### Alternative Google Font to Noto Font is EB Garamond and Merriweather
* "The elegant EB Garamond is a fantastic alternative for Times New Roman." 
* [EB Garamond@google_font](https://fonts.google.com/specimen/EB+Garamond#standard-styles)
* [EB Garamond@tug](https://tug.org/FontCatalogue/ebgaramond/)

```latex    
\usepackage[cmintegrals,cmbraces]{newtxmath}
\usepackage{ebgaramond-maths}
\usepackage[T1]{fontenc}
```
   
* Merriweather is softer and more casual than Times New Roman, yet still distinguished.
* [Merriweather@google_font](https://fonts.google.com/specimen/Merriweather#license)
* [Merriweather@tug](https://tug.org/FontCatalogue/merriweather/)

```latex
\usepackage{merriweather} %% Option 'black' gives heavier bold face 
\usepackage[T1]{fontenc}
```

### Alternative standard LaTeX Font to Times New Roman
* "The mission of the Scientific and Technical Information Exchange (STIX) font creation project is the preparation of a comprehensive set of fonts that serve the scientific and engineering community in the process from manuscript creation through final publication, both in electronic and print formats." 
* [stix2 homepage](https://www.stixfonts.org/)
* [Stix2@tug](https://tug.org/FontCatalogue/stix2/)

```latex    
\usepackage[T1]{fontenc}
%\usepackage{stix2} 
```

* https://tug.org/FontCatalogue/ebgaramond/
* https://github.com/stipub/stixfonts
* https://en.wikipedia.org/wiki/STIX_Fonts_project
* https://tug.org/FontCatalogue/stix2/
* https://tug.org/FontCatalogue/stix/
* https://tug.org/FontCatalogue/notoserif/

[learnui]: https://learnui.design/blog/times-new-roman-similar-fonts.html

## Heading

* Heading 1: 14pt, bold
* Heading 2: 12pt, bold, numbering [integers][.]
* Heading 3: 12pt, bold, numbering [integers][letters][.]
* Normal   : 12pt, regular
* itemize bullets : black dots
* table headers color: dark gray (hex 666666; RGB 102,102,102)

    
## Colors

 


# Gitlab CI pipeline for LaTeX example

This repository contains examples for the blog post 
[How to annoy your co-authors: a Gitlab CI pipeline for LaTeX](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/).

This latex-pipeline is using 
[Docker](https://docs.docker.com/get-docker/) 
and 
[GNU make](https://www.gnu.org/software/make/)
together with 
[latexMK](https://ctan.org/pkg/latexmk?lang=en) 
in a the 
[texlive:latest container](https://hub.docker.com/r/texlive/texlive).

The [texlive:latest container](https://hub.docker.com/r/texlive/texlive).
is updated weekly by the texlive organisation.


When you need to change the (advanced) setting, you can do this via the `Makefile` and `latexmkrc` files.

---

Compile locally with 

```bash
make clean render
```

or 

```bash
make clean render LATEXMK_OPTIONS_EXTRA=-pvc
```

to keep compiling the pdf when the input files are updated.

To refresh the PDF continuous:
```bash
evince paper/latexmk/main.pdf
```

This runs the PDF viewer [Evince](https://wiki.gnome.org/Apps/Evince) 


## Combine GitLab with Overleaf
* see [this wiki page](https://gitlab.com/edzob/antifragile-research/-/wikis/docs/Overleaf-and-Gitlab) on the Antifragile MSc wiki on how to configure Gitlab and Overleaf into the same project folder on your laptop.