%!TEX root = ../main.tex

\subsection{Elaboration 
of the core concepts 
(Theoretical analyses; 
Stipulative definitions; 
Reference list – APA Style) 
– 2,500 words.}

\begin{xltabular}{\textwidth}{ |X| } 
\hline
\textbf{The value of information and relevance of information security}\\
Information serves as a critical resource for organizations, 
guiding decision-making, actions, and delivering services and products. 
This underscores the significance of information security across all organizational domains. 
However, defining information security 
and achieving it 
is not a straightforward task 
\citep{schneier_psychology_2008, 
       smith_towards_2023}.
The challenge here is that the value of information is situational, 
since information is data placed into context 
\citep{van_gils_data_2023},
where context is subjective and continuously changing 
\citep{heyd_spoilers_2018, 
       hutchins_iso_2018, 
       black_investigating_2023, 
       kok_leading_2018, 
       mack_managing_2015, 
       mintzberg_rise_1994}.
Data itself is the materialization of an observations  
and thus fragile to perspective and perception.
The (implicit) selected viewpoint on how the world works,
determines the perspective to see the world, 
and influences the value of the stored data, 
and thus impacts the how and what of information security (fig 03). 

Examples of the possible viewpoints are:
• Data as reflection of our mutual synchronization of our reality
    \citep{downes_models_2009, 
           hoffman_interface_2015};
• Data as reflection by the language we speak 
    \citep{mykhailyuk_languages_2015,
           spivey_how_2012,
           louwerse_keeping_2021,
           lupyan_linguistically_2012,
           lleras_metaphors_2014};
• Data as reflection of the language used for interaction
    \citep{hestenes_notes_2006,
           ogden_meaning_1923,
           stamper_semiotic_1991};
• Data as reflection of our internal mental models 
    \citep{hestenes_notes_2006,
           qin_predictability_2023,
           taleb_black_2007,
           johnson-laird_mental_2010,
           ciborra_labyrinths_2004};
• Data as reflection of social interactions
    \citep{habermas_theory_1986,
            van_reijswoud_structure_1996,
            whalen_transaction_2011}.    
\\
    {\begin{tabularx}{\columnwidth}{C}
        \includegraphics[width=0.2\linewidth, fbox]{data}
        \\
        {\footnotesize Figure 03: 
        Information is data placed in context. 
        Value of information is dependent on the context.}
        \\
        {\footnotesize
        \citep{van_gils_data_2023}}
    \end{tabularx}}
\\
\textbf{Changing value perception and organizational risk management}\\
The choice for a viewpoint on how the world works 
and thus on what the value of data is, is not trivial choice.
This choice is a personal one and probably also non-exclusive 
to one choice and impacts how to secure information.
In the domain/ profession of information security, 
there's a prevailing notion that determining the value of data and information,
is the first step towards implementing appropriate security measures. 
This approach of determining value and then determining the security activities,
is influenced by the dynamic nature of organizational contexts and 
therefore the value of data in the perception of the various stakeholders 
is changing continuously.  
\\
The concept of that the context inside and outside an organization 
is changing continuously is as old as 
Heraclitus\footnote{\url{https://iep.utm.edu/heraclit}} 
(fl. c. 500 BC) 
\citep{heyd_spoilers_2018}.
Being exposed and being part of continuous change
is a challenge for organizations 
since for them it is important to make sense 
of their internal and external context 
and adapt accordingly, their value proposition 
\citep{osterwalder_business_2004}
and their internal construction 
\citep{janssen_enterprise_2015,
       dietz_enterprise_2020},
to stay relevant to 
all their stakeholders
\citep{hoogervorst_foundations_2018,
       mintzberg_rise_1994,
       op_t_land_enterprise_2009}.
\\       
It is known that we are not successful in 
changing organizations 
\citep{mintzberg_fall_1994}, 
products 
\citep{black_investigating_2023}, 
or IT systems
\citep{schmidt_mitigating_2023, schmidt_it_2023},
when it is approached from a big design upfront.
Changing the organisation entails a large amount 
of resources.
Not only the change of value perception is a risk for the organization, 
adapting to the change also is an organizational risk 
\citep{aven_enterprise_2019}.
Making sense of the internal and external context
of an organization in relation 
to the investment decisions of the organization itself
is part of risk management (RM)
\citep{hiles_enterprise_2012, 
       hiles_definitive_2010,
       aven_enterprise_2019,
       eling_cyber_2021,
       black_investigating_2023}. 
Challenges for organizations is that the investment of time
and money, also known as resources, are limited
\citep{aven_enterprise_2019,
       motet_illusion_2017}.
Risk management and security are closely connected 
since both address that what is value in perception 
of the stakeholders in the organisation.
\\
\textbf{From Security to Information Security, 
Cyber Security and Cyber Resilience}\\
There are formal and informal descriptions of 
what security is.
There are descriptions that distinguish 
between the security of people, physical objects, money, 
information, business continuity and so on
\citep{brooks_what_2010,
       eling_cyber_2021,
       schneier_psychology_2008}.
Next to the variety of topic to secure, 
there is is also a wide range of possible ways 
to look at what value is that needs to be secured. 
% (axiology)
These two constructs combined 
impact the words that are used to describe 
the view on how action impacts value. %(etiology)
For example combinations as
financial security, health security and physical security.
When focusing on information and information technology,
the following words are commonly used:
computer security, 
IT Security, 
information security (infosec), 
information security risk management, 
Information Assurance (IA),
cyber security, 
cyber and information security (CIS),
cyber risk management, 
cyber resilience
\citep{eling_cyber_2021, 
       sulaiman_cyberinformation_2022, 
       portela_organizational_2014,
       schatz_towards_2017}.
\\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\textbf{Defining Security}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The definition of security itself
is depending on the context,
and the view on the domain %(ontology)
and the motivation %(teleology) 
influences the definition of the word security
\citep{brooks_what_2010}.
This is why we like to adapt 
the following definition of security
"Security is both a feeling and a reality."
(fig 04)
\citep{schneier_psychology_2008}.
Security is where your feeling overlaps
with the calculation of predicted occurrences in reality
\citep{schneier_psychology_2008}.
We would like to extend this definition to 
that security is the overlap in the venn-diagram of 
a circle representing your personal expectation and
another circle representing your observation of the materialization
in the reality (fig 05)
\citep{huang_perception_2010}.
The shift from calculation toward observation 
extends the definition of reality into
security is where your feeling overlaps
with the calculation of predicted and observed occurrences in reality.
\\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\textbf{Defining Information Security}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
As stated before, 
information plays a crucial role 
in the value creation of organizations,
providing relevance to the term of Information Security
and places information security in the domain of Risk Management.
\\
    {\begin{tabularx}{\columnwidth}{CC}
        \includegraphics[width=0.4\linewidth, fbox]{venn}
        &
        \includegraphics[width=0.4\linewidth, fbox]{vennv2}
        \\
        {\footnotesize Figure 04: Security Venn diagram} 
        &
        {\footnotesize Figure 05: Security Venn diagram}
       \\
       {\footnotesize
       \citep{schneier_psychology_2008}}
       &
       {\footnotesize
       \citep{schneier_psychology_2008,huang_perception_2010}}
    \end{tabularx}}
\\
Various researchers have identified 
the lack of clear definition of Information Security
\citep{von_solms_information_2013, 
       eling_cyber_2021, 
       sulaiman_cyberinformation_2022, 
       farid_digital_2023,
       ahouanmenou_information_2023,
       anderson_why_2003,
       portela_organizational_2014} 
supporting the conclusion
that the definition of security itself
is depending on the context
\citep{brooks_what_2010}.
\\
We adopt the definition
of Information Security as
"a continuous process in a system 
that aims to maintain 
availability [A], 
confidentiality [C] and 
integrity [I]
of the data stored and communicated by the system"
\citep{moallem_hci_2020}.
This definition limits Information Security 
to the CIA attributes of a system, 
where we define CIA as the outcome of a continuous process, 
and using the term data in stead of the term information.
Using the term data reduces the exposure to the context
defining the value of the information.
The usage of a continuous process
is important since the complexity achieved 
by the open integration of 
IT systems, business processes and organizations 
leads to the behavior of IT’s being nondeterministic 
\citep{boeing_visual_2016, 
       derbyshire_preparing_2014, 
       lorenz_deterministic_1963}. 
This nondeterministic behavior
can not be compensated by 
deterministic checklists and deterministic controls
\citep{heylighen_complexity_2016, 
    hoogervorst_foundations_2018}.
\\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\textbf{Defining Cyber Security}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The term Cyber Security has no unique definition, 
and distinguishes itself from information security 
via the term Cyber 
referring to Cyberspace and Cyber-physical
\citep{eling_cyber_2021,
       maillart_heavy-tailed_2008,
       kriek_technobody_2018},
increasing the scope from information security 
to the entirety of the organizational security
\citep{schatz_towards_2017,
       sulaiman_cyberinformation_2022,
       von_solms_information_2013, 
       kott_fundamental_2019}. 
This increased scope follows our current reality 
of complex integration of 
complicated internal Information Systems
\citep{schwarzkopf_virtual_2015,    
       tanenbaum_distributed_2002,
       hohpe_enterprise_2004, 
       wirth_brief_2008}, 
which has helped the evolution to a complex integration 
of internal and external Information Systems as well 
as of internal and external business processes
\citep{lapalme_three_2012,
       ross_designed_2019,
       gordijn_business_2023,
       strikwerda_organized_2023}.
We adopt the following definition of cyber security:
"Cybersecurity is the organization and collection
of resources, processes, and structures
used to protect cyberspace and cyberspace-enabled systems
from occurrences that misalign
\textit{de jure} from
\textit{de facto} property right."
\citep{craigen_defining_2014,
       collard_definition_2017}.

This definition does not explicit include risk management, 
as the confidentiality, integrity and availability of data, 
and does explicit focus on protect and not on recover from or exploitation of events.
Cyber resilience does entail risk management 
and recovering from events 
\citep{eling_cyber_2021,
       kott_fundamental_2019}.
\\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\textbf{Defining Cyber Resilience}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In addition to the non-consensus on security, 
information security, and cyber security,
the term cyber resilience 
faces the additional challenge of the term resilience 
not having a clear definition 
\citep{martin-breen_bellagio_2011,
    beukenkamp_securing_2016,
    smith_towards_2023,
    hosseini_review_2016,
    klein_resilience_2003,
    cottam_defining_2019}.

The challenge of
what is meaning, %(ontology), 
and how do we gain 
purposefully %(teleology) 
knowledge %(epistemology)
plays an important role
in the context of cyber resilience.
We embrace the following definition of
resilience as
"Resilience specifically 
is the elastic response to stress 
where the system is measurably impacted
but returns to the former state 
when the stress is removed or nullified" 
and for new we follow as definition of cyber resilience
"Cyber resilience 
is the ability of a cyber system to recover
from stress that causes a reduction of performance"
\citep{smith_towards_2023}.
\\
We find it interesting that in the available definitions of cyber resilience,
the focus is on the recovery to the former state, 
and is excluding the definitions of Complex Adaptive System (CAS) resilience
where the new normal can be a state of higher 'value'
\citep{martin-breen_bellagio_2011} (fig 06).
CAS Resilience is the capability
to reconfigure the function (delivered service) 
and the construction (delivering the service)
to exceed the value of the former state
\citep{beukenkamp_securing_2016, 
       botjes_attributes_2021}. 
As addressed in the section on Risk Management 
is the exploitation of an event 
to exceed the former state 
part of ISO 31000 (Risk Management)
\citep{hutchins_iso_2018}.
This would hint to room for extending 
the definition of Cyber Resilience 
and introducing the definition of Antifragility 
as optimization of CAS resilience.
Optimization of CAS resilient behavior  
is relevant since information security 
is a continuous process. 
Including CAS resilience also fits the
positioning of Cyber Resilience as a socio-technical concept
\citep{dupont_tensions_2023,
       colabianchi_discussing_2021}.
\\
    {\begin{tabularx}{\columnwidth}{CCC}
   \includegraphics[width=0.5\linewidth, fbox]{3types}
        \\
        {\footnotesize Figure 06: Three types of resilience
        \citep{martin-breen_bellagio_2011}}
    \end{tabularx}}
\\
\textbf{From Risk Management to Cyber Security and Cyber Resilience}\\
This dynamic and unpredictable landscape,
expose organizations constantly  
to the risk of disruptions and cyber attacks,
posing unprecedented challenges to their cyber resilience
\citep{eling_cyber_2021, 
       aven_enterprise_2019}.
Challenges for organizations 
is that the investment of time and money, 
also known as resources, 
is limited. 
\citep{aven_enterprise_2019,
       motet_illusion_2017}. 
Making sense of the 
(unpredictable) 
internal and external context
of an organization in relation 
to the investment decisions of the organization itself
is part of Risk Management (RM)
\citep{hiles_enterprise_2012, 
       hiles_definitive_2010,
       aven_enterprise_2019,
       eling_cyber_2021,
       black_investigating_2023,
       motet_illusion_2017}. 
\\
The concept of 'uncertainty',
as part of VUCA,
is integrated into
the ISO standards
ISO 31000:2018 [Risk Management] and
ISO 9001:2015 [Quality Management Systems]
\citep{hutchins_iso_2018}.
The field of Risk Management
is adopting the name of
Cyber onto Resilience as Cyber Resilience
\citep{eling_cyber_2021,
       smith_towards_2023,
       maillart_heavy-tailed_2008,
       kriek_technobody_2018},
due to the complexity of organizations
as combination of technology 
and social concepts
\citep{jackson_critical_2019,
       heylighen_science_2002,
       lapalme_enterprise--environment_2014},
and to acknowledge the important role of
the objective as well as the subjective viewpoint
when addressing 'uncertainty' and 'unpredictability'
\citep{fjaeran_creating_2021}.
\\
\textbf{Organizational Resilience and Antifragility}\\
Organizational resilience refers to the time 
an organization needs to absorb and recover from disruptions,
and refers to the organizational capability
to determine, decide and act
\citep{martin-breen_bellagio_2011,
       mckay_designing_2020,
       boulton_embracing_2015,
       annarelli_understanding_2020,
       mendoza_framework_2021}.
Resilience in general 
can be seen as the time needed to absorb and recover from an event
\citep{botjes_attributes_2021,
       smith_towards_2023} (fig 07).
Since the internal and external context of
organizations are constant in change, 
organizational resilience plays a role in keeping an organization relevant 
\citep{aven_enterprise_2019,
       aven_foundational_2021}.
How an organization delivers value,
we call the construction.  
Which service an organization delivers, 
we call the function. 
For organizational resilience 
to exploit an opportunity
is linked to the change of
the function and the construction
of the organization 
\citep{botjes_attributes_2021,
       martin-breen_bellagio_2011} (fig 08).
Optimal resilience is achieved 
when an organization is optimized 
for the change of function and construction (fig 09).
Optimization for this is called
Antifragility
\citep{taleb_antifragile_2012, 
       mendoza_framework_2021, 
       botjes_attributes_2021,
       aven_concept_2015}. 
The value of Antifragility is that 
when optimized for continues change,
and exploitation of events, 
the organization is prepared to deal with
events and become stronger from these events, 
even if the event is unforeseen.
It is argues that being Antifragile is 
a way to not be disrupted by the events that are unknown unknown, 
also coined x-events or black swan events
\citep{botjes_attributes_2021}.
Since the internal and external context of an organization is in continues flux,
and value perception is also in continuous change, 
Antifragility is relevant for organizations and all its stakeholders.
\\
    {\begin{tabularx}{\columnwidth}{CCC}
          \includegraphics[width=1.1\linewidth, fbox]{resilience}
        & \includegraphics[width=0.5\linewidth, fbox]{casresilience}
        & \includegraphics[width=0.55\linewidth, fbox]{antifragile}
        \\
          {\footnotesize Figure 07: Generic definition of resilience
          \citep{botjes_attributes_2021}}
        & {\footnotesize Figure 08: Resilience achieved by changing function and construction
        \citep{martin-breen_bellagio_2011}}
        & {\footnotesize Figure 09: Antifragile
        \citep{botjes_attributes_2021}}
        \\
    \end{tabularx}}
\\
To achieve the change 
of the function and the construction 
of an organization,
the change needs to include 
the social (coordination, affordance) 
as the technological 
(production) aspects since they both influence
data, information and action
\citep{lapalme_enterprise--environment_2014, 
       volkoff_critical_2013, 
       kotusev_can_2020, 
       hoogervorst_foundations_2018}.
\\
Since today's world is hyper-connected, 
and organizations are seamlessly interwoven 
into a complex network of information, data, and processes, 
\citep{weiss_cyber_2023,
       saeed_digital_2023,
       schwarzkopf_virtual_2015,
       sadiq_artificial_2021, 
       wirth_brief_2008, 
       eling_cyber_2021}
resulting in an environment 
for organizations that is characterized as
Volatility, Uncertainty, 
Complexity, and Ambiguity (VUCA)
\citep{bennett_what_2014, 
       bennett_what_2014-1, 
       mack_managing_2015, 
       hutchins_iso_2018, 
       oreilly_no_2019,
       black_investigating_2023}.
“Nearly all nontrivial real-world systems 
are nonlinear dynamical systems.”
\citep{boeing_visual_2016}.
To survive our VUCA context,
our design of social and technological part of our organizations
should incorporate antifragile behavior
\citep{taleb_antifragile_2012, 
        aven_concept_2015,
        botjes_attributes_2021, 
        eling_cyber_2021}.
\\
\textbf{Cyber Resilience Maturity Models}
\\
Managing risks and seize opportunities,
even if they are known or unknown 
is the goal of Enterprise Risk Management
\citep{hutchins_iso_2018,
       aven_enterprise_2019}
also known as Cyber Resilience 
\citep{eling_cyber_2021}. 
We have argued:
•  That information is crucial in the 
value creation of an organization and that the value of 
this information is context depended;
•  That the interconnected, internal and
external, business processes and information systems 
lead to the need for Risk Management and Cyber Security
to combine in Cyber Resilience;
•  The ability for an organization to evolve 
is part of resilient and cyber resilient behavior;
\\
We also are going to argue that
•  Maturity Models are a tool to support
organizations in developing a road-map to evolve their maturity;
•  Maturity Models support the learning organization;
•  For a Cyber Resilience Maturity Model (CRMM), the CMMI classification of the maturity levels does not work;
•  Maturity Models are known to lack in quality; and
•  No existing CRMM appear to address
the capability to exploit unforeseen events, 
also know as antifragile organizational behavior.
•  No existing CRMM appear to address the learning organisation
to the extend of adapting the function and construction of the value preposition
within the addressed scope.
\\
This why our research aims to preferable extend an existing CRMM,
and are realistic about the change a new CRMM needs to be designed, implemented and validated.
\\
\textbf{6. Maturity Models and road-mapping}\\
Organizations are complex systems
and can be classified as complex adaptive systems 
\citep{jackson_critical_2019,
       metcalf_handbook_2021, 
       heylighen_science_2002, 
       dietz_discipline_2013}.
Maturity models are created as a tool, 
to guide management of organizations 
with discrete maturity levels for a 
class of objects representing stages of capabilities 
(qualitative and/or quantitatively)
\citep{becker_developing_2009}.
Guidance is provided via evaluation of current state 
and thus leading to insight 
in progress as in providing input for a road-map 
to go from the current state to the future state
\citep{de_bruin_understanding_2005,
       becker_developing_2009,
       becker_maturity_2010,
       hutchison_design_2010,
       poeppelbuss_maturity_2011}.
Maturity models are  
common in the hundreds 
in various fields within business and academia
\citep{monteiro_maturity_2020,
       bergener_need_2019,
       becker_developing_2009,
       van_steenbergen_maturity_2011}.
A maturity model consists of a sequence of maturity levels 
for a class of objects. 
It represents an anticipated, desired, 
or typical evolution path 
of these objects shaped as discrete stages. 
Typically, these objects are organizations or processes
\citep{becker_developing_2009}.
Maturity models are action-oriented standards 
that consist of discrete maturity levels 
for a class of processes or organizations. 
It represents the stages of increased quantitative 
or qualitative capability changes 
of these processes or organizations 
to evaluate their progressions concerning 
defined focus areas
\citep{sadiq_artificial_2021}.
Maturity models are created around various constructs,
as for example:  
• Staged fixed-level models 
(incl. Capability Maturity Model for Software (CMM), 
Nolan’s Stage of Growth Model, Crosby’s Grid), 
• Continuous fixed-level models 
(incl.  Capability Maturity Model Integration (CMMI))
and
• Focus area models
(incl. Test Process Improvement, TPI and 
Dynamic Architecture Maturity Model, DYAMM)
\citep{monteiro_maturity_2020,
       roglinger_what_2011,
       van_steenbergen_maturity_2011,
       verheijen_maturity_2023,
       lasrado_set-theoretic_2018}.
In most maturity models the CMMI scale is used.
The CMMI levels are: incomplete (0), initial (1),
managed (2), defined (3), quantitatively managed (4),
optimizing (5).
In our opinion the function of a Maturity Model
to guide context specific actions
can not be achieved using a CMMI scale.
Since a VUCA context will hinder completing the defined level,
or the quantitatively managed level.
Therefor we need to explore the other constructs.
\\
Overall Maturity Models are used as a vehicle
to summarize the known body of knowledge
and are a visual representation
of researcher's mental model.
This leads to a high variation in Maturity Models.
The variation in Maturity Models
and lack of quality in these models
leads to the identification of the need for 
a maturity model for maturity models
and that of an architecture
for maturity models
\citep{monteiro_maturity_2020,
      bergener_need_2019,
      kohlegger_understanding_2009,
      buckle_maturity_2018,
      pereira_review_2020,
      normann_andersen_ten_2020, 
      thordsen_decade_2023}.
The main function of a maturity model
is that to support road-mapping,
and this function is also in need of a maturity model
\citep{hillegas-elting_roadmapping_2016}. 
\\
This implies that in our research
we first need to summarize the critique on maturity models,
secondly we need to capture the existing critiques
as criteria to hold the found Cyber Resilience Maturity Models against,
and thirdly translate the critiques into research activities to in the end create
a maturity model that can uphold against the know critiques.
\\
\textbf{Relation between road-mapping and resilience is learning}
\\
A value of road-mapping
is that it supports evaluation.
The evaluation of the progression, 
a.k.a. feedback-loops,
play an important role in 
the flexibility and self-healing properties 
of an organizational as well a socio-technical system 
is a form of learning
\citep{vamvoudakis_control_2016,
       wijnhoven_organizational_1995,
       huang_reinforcement_2022,
       brons_kringelum_critical_2021,
       senge_fifth_1990}.
Observing the change in our context 
and processing this is what fuels our personal learning
\citep{raviv_how_2022,
       hestenes_conceptual_2015,
       hoogervorst_foundations_2018}
for example by using tools like 
Plan-Do-Check-Act (PDCA), 
Observe, Orient, Decide and Act (OODA)
and Cynefin
\citep{metcalf_handbook_2021, 
       strikwerda_organized_2023}.
Organizational learning has the contributed function
of knowledge creation, acquisition and integration
to achieve the sustainability of competitive advantage 
\citep{alashwal_influence_2017,
       lopez_human_2006,
       hutchison_facilitating_2005}.
As stated before, 
to keep the competitive advantage sustainable 
it is important for an organization 
to deal with the unknown.
It makes sense that organizational learning 
is an integral part of organizational resilience
and is relevant from the board room
\citep{aven_knowledge_2018,
       aven_enterprise_2019,
       park_integrating_2013,
       aven_conceptual_2017},
to the operations
\citep{huang_reinforcement_2022}.
\\
In previous research \citep{botjes_attributes_2021}
we have identified that organisation learning is a
common factor for resilience and Antifragility.
Also the model of organizational learning
provided by \cite{senge_fifth_1990},
is identified as a useful model.
The role of this model needs to be challenged by
more recent research on organisation learning,
and hold against learning organisation components in existing CRMMs'.
A quick scan of the found CRMMs' and feedback from researchers and practitioners in cyber security
provide the insight that the current CRMMs' do not incorporate organisational learning.
\\
\hline
\end{xltabular}
