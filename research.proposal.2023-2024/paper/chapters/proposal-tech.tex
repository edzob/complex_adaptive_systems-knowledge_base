%!TEX root = ../main.tex

\subsection{Technical Research Design and Methods
(Project Type; Research strategy;
Quantitative: population/sample;
Qualitative: cases selection; data-analysis techniques)
(Methods of credible and controllable data gathering
should eventually be described in enough detail,
so that the PhD study can be replicated by fellow researchers)
– 500 words.}

\begin{xltabular}{\textwidth}{ |X| } 
\hline
\textbf{Research Lens - Cyber Resilience Maturity Models and the socio-technical domain}\\
Embarking onto research in the 
topic of Cyber Resilience implies 
doing research in the socio-technical domain
\citep{dupont_tensions_2023,
       colabianchi_discussing_2021}.
Research in the social domain is challenging 
\citep{howell_introduction_2013, 
       recker_scientific_2012, 
       bell_business_2022}
regarding generalization and replication
\citep{kalelioglu_replicability_2021,
       stevenson_cause_2023,
       pyle_knowledge_2023}.
Replication research is needed to proof
the causal relationship between the actions and result.
The lack of research on the causality of actions and result
is a critique on Maturity Models
\citep{monteiro_maturity_2020}.
\\
The challenge of replication is often linked to
doing research in an open complex system,
be it a social system or a technological system.
Critical Realism as a research lens
offers a way to address 
the socio-technical construct
\citep{uppstrom_designing_2017}
and respecting the replication crisis 
\citep{tsang_replication_1999}
and the generalization challenge 
\citep{edwards_studying_2014}
relevant in the social science domain.
Within critical realism the position is that there
are things in the reality (objective) and observation of the real (subjective)
and that to discover the unobservable structures,
retroduction is applied.
Critical Realism is applied
in the fields of:
Information Science
\citep{javidroozi_qualitative_2018, 
       galliers_affordance_2017}, 
organizational studies
\citep{edwards_studying_2014},
design science
\citep{uppstrom_designing_2017},
and action research
\citep{looker_case_2021, 
       edwards_studying_2014}. 
\\
The design of our Information Science research
will look through the Critical Realism research lens
at Cyber Resilience as 
the organizational capabilities 
of Risk Management and Cyber Security
reflected on the research method of 
design science.
This results in the need for the research approach to
be able to deal with wicked problems by supporting iterative research cycles and
supporting retrospective analysis of the problem domain and theory building. 
\\
\textbf{Research Design}
\\
The method of inquiry we intend to use is
elaborated Action Design Research (eADR) 
\citep{mullarkey_elaborated_2019} 
which integrates Design Science Research (DSR)
\citep{hevner_three_2007, 
       hevner_design_2004, 
       drechsler_four-cycle_2016}
and Action Design Research (ADR)
\citep{sein_action_2011, susman_assessment_1978}
and is developed for application to wicked problems
\citep{mullarkey_elaborated_2019}. 
Within each of the eADR cycles (fig 12)
the ADR intervention cycle steps: 
Problem Formulation,
Artifact Creation, 
Evaluation,
Reflection and
Formalization of Learning
are placed.
It is possible to go forward and go back
between the eADR cycles during the research.
The eADR enables to research to 
go back to the first cycle,
making this method suited for wicked problems and
the critical realist approach.
\\
{\begin{tabularx}{\textwidth}{C} 
       \includegraphics[width=0.9\linewidth, fbox]{elaborated ADR RQ}   
       \\
       {\footnotesize Figure 12: Research Questions mapped on Elaborated ADR (eADR)}
       \\ 
       {\footnotesize addopted
       \citep{mullarkey_elaborated_2019}}
       \\
\end{tabularx}}
\\     
\textbf{RQ1: Is there a Cyber Resilience Maturity Model (CRMM) that adequately addresses the exploitation of
unforeseen events, as defined in the ISO 31000 on risk management?}
\\
\textbf{Diagnosis Cycle -}
The artifact of the diagnoses cycle will be 
a literature study containing the criteria
and application of these criteria on the found CRMM's. 
Fellow researchers and practitioners 
will participate in the identification of existing CRMMs. 
Their input will be used to optimize 
the systematic literature research search query. 
Their input will be asked for the criteria 
for a sound Maturity Model and CRMM.
The involvement will mitigate the subjectivity of the research team.
When the set of criteria to the Maturity Model 
and the CRMM is deemed mature enough 
it will be validated by experts and practitioners,
before it is applied as filter on the discovered existing CRMMs.
The Diagnosis step will be completed with a peer-review article.
\\
\textbf{Design Cycle -}
The artifact of the design cycle will be 
the selected CRMM with (potentially)
adjustments based on the criteria 
retrieved from the diagnosis cycle.
The existing CRMMs will be validated 
against the published set of Maturity Model criteria 
and CRMM criteria. 
This evaluation will provide per CRMM 
a list of strong points and short comings.
This list will be validated by experts.
Feedback from the experts might evoke a revert back
to the diagnosis cycle.
\textit{Important:} Current research already identifies 
that no existing CRMM will comply with 
(1) the already known criteria of causality, 
(2) the learning organization and 
(3) the capability of exploitation (Antifragility).
Therefore it is inevitable to change an existing CRMM,
or create a new CRMM, 
when the gap between the existing CRMMs and the criteria is too big.
The vision for the creation of a CRMM
is to start small and validate first the structure
before creating a complete CRMM. 
At the end the new or improved CRMM 
will be validated by a group of experts,
and the end result of the selection and creation 
will be published in a peer-review journal (table 01).
\\
{\footnotesize
    \begin{tabularx}{\linewidth}{|l|l|l|X|c|c|}
    \hline
    \textbf{RQ}   & \textbf{eADR} & \textbf{ADR} & \textbf{Method} & \textbf{QUAL} & \textbf{QUAN} \\
    \hline          
     1.1-1.5     & Diagnose & Problem Formulation        & Theory Building                    & Y &   \\
                 &          &                            & Expert interviews for exploration  & Y &   \\
                 &          & Artifact Creation          & Systematic Literature review       &   & Y \\
                 &          & Evaluation                 & Focus group for validation         & Y &   \\
                 &          & Reflection                 & Revisit Theory Building            & Y &   \\
                 &          & Formalization of Learning  & Peer review for validation         & Y &   \\
    \hline
    1.6          & Design   & Problem Formulation        & Identify shortcomings and strong points & Y &   \\
                 &          &                            & Expert interviews for validation        & Y &   \\
                 &          & Artifact Creation          & Design new and improved CRMM            &   & Y \\
                 &          & Evaluation                 & Focus group for validation              & Y &   \\
                 &          & Reflection                 & Revisit Theory Building                 & Y &   \\
                 &          & Formalization of Learning  & Peer review for validation              & Y &   \\
    \hline
    \multicolumn{5}{c}{Table 01: RQ1 - eADR, ARD Intervention, method}\\
    \end{tabularx}
}
\\
\textbf{RQ2: Does the (extended) Cyber Resilience Maturity Model (CRMM) offer guidance and value when
applied by organizations?}
\\
\textbf{Implementation Cycle -}
In the implementation cycle 
the application of the created CRMM 
will be researched by application in the reality. 
For this there needs to be found multiple organization
with each multiple teams,
that is responsible for a product or service 
within IT or business, that delivers value,
and is challenged with internal and/or external 
unpredictability (VUCA).
\\
The earlier mentionded
European regulations
(NIS2, CRA, DORA, RED, MDR, PLR, \ldots,)
are great motivators for
companies to experiment with cyber resilience approaches, 
since the requirements and ways to implement 
will not be clear for many years. 
This obligation also results in a bias.
In the past a large online retailer, 
a large industrial manufacturer, and 
a large financial service company 
have showed interest in participating in the research.
The largest national railway company 
might also want to participate 
based on the mentioning of
Chief Resilience Officers 
in relation to the responsibilities of the 
CISO\footnote{\url{https://www.pvib.nl/actueel/blogs/ridder-slijp-je-veerkrachtzwaard}}\textsuperscript{,}\footnote{\url{https://cisocommunity.nl}}.
We  need to be realistic that in the end 
it is luck, timing and trust that 
determine which companies are able to collaborate at the
right time in the research.
We feel positive that multiple companies and multiple experts will like to participate in our research.
We see it as bonus if we can apply the CRMM onto multiple teams, 
and when it is possible to apply it over multiple organizations.
That way we can see the application in variations of organization size, sector, governmental vs commercial etc.
We also see it as bonus if it is possible to monitor the application 
of the CRMM over time at the test subjects.
\\
Part of the Implementation cycle is to define the research approach in the specific context,
in such a way that the application of the CRMM can be done without the influence of the main researcher,
and that the results over the teams and organizations (over time) can be compared.
We foresee that the implementation cycle needs a few iterations, including a few test (dry-run) experiments,
to be confident that the application in the reality can be used in the research (table 02).
\\
{\footnotesize
    \begin{tabularx}{\linewidth}{|l|l|l|X|c|c|}
        \hline
        \textbf{RQ}   & \textbf{eADR} & \textbf{ADR} & \textbf{Method} & \textbf{QUAL} & \textbf{QUAN} \\
        \hline
        2.1-2.3   & Implementation & Problem Formulation        & Design research approach           & Y &   \\
                  &                &                            & Research subject selection         & Y &   \\
                  &                & Artifact Creation          & Research at subjects               & Y &  Y \\
                  &                & Evaluation                 & Focus group for validation Research execution & Y &   \\
                  &                &                            & Expert group for reflection on results        & Y &   \\
                  &                & Reflection                 & Revisit research design            & Y &   \\
                  &                & Formalization of Learning  & Peer review for validation         & Y &   \\
                  \hline
        2.4       & Evolution      & Problem Formulation        & Design research approach           & Y &   \\
                  &                & Artifact Creation          & Periodic data-retrieval on application CRRM   &   & Y \\
                  &                & Evaluation                 & Focus group for validation Research execution & Y &   \\
                  &                &                            & Expert group for reflection on results        & Y &   \\
                  &                & Reflection                 & Revisit research design            & Y &   \\
                  &                & Formalization of Learning  & Peer review for validation         & Y &   \\
        \hline
        \multicolumn{6}{c}{Table 02: RQ2 - eADR, ARD Intervention, method}\\
    \end{tabularx}
}
\\
\textbf{RQ3: What are the key lessons learned from the application of the Cyber Resilience Maturity Model
(CRMM) and how can these lessons be incorporated in to refined version of the CRMM?}
\\
\textbf{Evolution Cycle -}
In the evolution cycle 
a review is done on the evolution of the artifact in its context and 
retrospective on the diagnosis of the problem domain. 

\textbf{Retroduction -}
In addition we are going to collect and create reflection on the 
ADR interventions cycles within the eADR process and the
eADR process as a whole, by revisiting the Diagnose Cycle in the 
eADR.
The assumption is that we can go through all the eADR cycles
without any impactful changes, and provide an slightly improved
artifact (CRMM) for the evolution cycle, 
proving that the artifact 
and application of the artifact are viable. 
The Critical Realism approach states that in the step of retroduction 
we can re-discover other structures that are relevant to the CRMM.
This step also demands the responsiblity of a clear versioning policy to the CRMM artifact,
since it will evolve in the reality and it the design will be revisited 
as result of going through all the eADR cycles.

RQ3 is the research question with the most unknowns,
since there is a possibility that a certain concept,
not identified in the theory building (RQ1) and not 
identified during the various expert and practitioners validations (table 03).
\\
{\footnotesize
    \begin{tabularx}{\linewidth}{|l|l|l|X|c|c|}
        \hline
        \textbf{RQ}   & \textbf{eADR} & \textbf{ADR} & \textbf{Method} & \textbf{QUAL} & \textbf{QUAN} \\
        \hline
        3.1       & Evolution      & Problem Formulation        & Theory Building                    & Y &   \\
                  &                &                            & Expert interviews for exploration  & Y &   \\
                  &                & Artifact Creation          & Systematic Literature review       &   & Y \\
                  &                & Evaluation                 & Focus group for validation         & Y &   \\
                  &                & Reflection                 & revisit Theory Building            & Y &   \\
                  &                & Formalization of Learning  & Peer review for validation         & Y &   \\
                  \hline
        3.2       & Diagnosis      & Problem Formulation        & Theory Building                    & Y &   \\
                  &                &                            & Expert interviews for exploration  & Y &   \\
                  &                & Artifact Creation          & Systematic Literature review       &   & Y \\
                  &                & Evaluation                 & Focus group for validation         & Y &   \\
                  &                & Reflection                 & revisit Theory Building            & Y &   \\
                  &                & Formalization of Learning  & Peer review for validation         & Y &   \\
                  \hline
        3.3       & Design         & Problem Formulation        & Theory Building                    & Y &   \\
                  &                &                            & Expert interviews for exploration  & Y &   \\
                  &                & Artifact Creation          & Systematic Literature review       &   & Y \\
                  &                & Evaluation                 & Focus group for validation         & Y &   \\
                  &                & Reflection                 & revisit Theory Building            & Y &   \\
                  &                & Formalization of Learning  & Peer review for validation         & Y &   \\
                  \hline
        3.4       & Implementation & Problem Formulation        & Theory Building                    & Y &   \\
                  &                &                            & Expert interviews for exploration  & Y &   \\
                  &                & Artifact Creation          & Systematic Literature review       &   & Y \\
                  &                & Evaluation                 & Focus group for validation         & Y &   \\
                  &                & Reflection                 & revisit Theory Building            & Y &   \\
                  &                & Formalization of Learning  & Peer review for validation         & Y &   \\
        \hline
        \multicolumn{6}{c}{Table 03: RQ3 - eADR, ARD Intervention, method}\\
    \end{tabularx}
}
\\
    \hline
\end{xltabular}   
