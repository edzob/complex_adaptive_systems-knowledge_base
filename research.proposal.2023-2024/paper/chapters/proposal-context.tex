%!TEX root = ../main.tex

\subsection{Project context (Background; Theoretical Lacuna; Practitioners’ Problems;
Societal Relevance–
please highlight the core concepts of your study)
– 300 words.}

\begin{xltabular}{\textwidth}{ |X| } 
\hline
\textbf{Societal relevance}
\\
In today's hyper-connected world, 
organizations are seamlessly interwoven 
into a complex network of information, data, and processes, 
\citep{weiss_cyber_2023,
       saeed_digital_2023}
resulting in an environment characterized by 
Volatility, Uncertainty, Complexity, and Ambiguity (VUCA)
\citep{bennett_what_2014, 
       bennett_what_2014-1, 
       mack_managing_2015, 
       hutchins_iso_2018, 
       oreilly_no_2019,
       black_investigating_2023}. 
This dynamic and unpredictable landscape,
expose organizations constantly  
to the risk of disruptions and cyber attacks,
posing unprecedented challenges to their cyber resilience
\citep{eling_cyber_2021, 
       aven_enterprise_2019}.
The escalating frequency and severity of cyber incidents, 
such as 
the Colonial Pipeline ransomware attack
\citep{reeder_cybersecuritys_2021},
Microsoft's exposure of AI training data\footnote{\url{https://www.wiz.io/blog/38-terabytes-of-private-data-accidentally-exposed-by-microsoft-ai-researchers}}
\citep{humphreys_ai_2024},
the Crowdstrike incident
\footnote{\url{https://cyberplace.social/@GossiTheDog/112811874544809589}}
and
hospital breaches of patient medical records
\citep{al_kinoon_comprehensive_2024},
underscore the threat to society
as the urgent need for effective resilience strategies.
These incidents not only disrupt operations 
but also have far-reaching societal consequences, 
impacting economies, healthcare systems, and public trust
\citep{schneier_psychology_2008, 
       smith_towards_2023}.
\\
\textbf{EU and US mitigate the threat to society by legislation}
\\
The EU tries to mitigate this threat to society
with various legislation\footnote{Network and Information Security 2 (NIS2): \url{https://digital-strategy.ec.europa.eu/en/policies/nis2-directive}}\textsuperscript{,}\footnote{Radio Equipment Directive (RED): \url{https://ec.europa.eu/commission/presscorner/detail/en/ip_21_5634}}\textsuperscript{,}\footnote{The Cyber Resilience Act (CRA): \url{https://digital-strategy.ec.europa.eu/en/library/cyber-resilience-act}}\textsuperscript{,}\footnote{Digital Operational Resilience Act (DORA): \url{https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex:52020PC0595}}\textsuperscript{,}\footnote{Product Liability Directive (PLD): \url{https://www.europarl.europa.eu/RegData/etudes/BRIE/2023/739341/EPRS_BRI(2023)739341_EN.pdf}}\textsuperscript{,}\footnote{Medical Devices Regulation (MDR) \url{https://health.ec.europa.eu/document/download/b23b362f-8a56-434c-922a-5b3ca4d0a7a1_en}}\textsuperscript{,}\footnote{European Cybersecurity certification framework: \url{https://digital-strategy.ec.europa.eu/en/policies/cybersecurity-act}}
under the Digital Decade\footnote{\url{https://digital-strategy.ec.europa.eu/en/policies/europes-digital-decade}}
program addressing Cyber Resilience
\citep{vostoupal_legal_2023,
       tasheva_hyperconnected_2022}.
Within the US the same movement is ongoing via national and state legislation
\citep{reeder_cybersecuritys_2021,
       carroll_us_2024}.
These legislations are based on existing risk management standards and frameworks.
Most risk management approaches,
focus on sequential steps,
for example the NIST Cyber Security Framework (CSF v2.0)
\citep{national_institute_of_standards_and_technology_nist_2023}
applies the functions
identify, protect, detect, respond and recover, 
which are similar to the process defined in the
Risk Management standards
ISO 27005, ISO 3100, NIST 800-30 and NIST 800-37
\citep{hutchins_iso_2018,
       cerqueira_junior_cyber_2023,
       putra_integrated_2023,
       joint_task_force_transformation_initiative_guide_2012}
which use:
context establishment,
risk assessment,
risk identification,
risk estimation,
risk evaluation,
risk treatment and
risk monitoring.
\\
\textbf{Mitigation does not fit the problem domain}
\\
Implementation of Risk Management
and that of Cyber Security
often take place in the context of
either the ISO 27001
\citep{culot_isoiec_2021,
       roy_high-level_2020}
       or the NIST SP 800-53
\citep{joint_task_force_interagency_working_group_security_2020}
standards for Information Security Management,
as reaction on the earlier mentioned legislations.
Within these standards the risk management steps
are reduced to checklists of implemented controls.
The approach of capturing the steps into a checklist is
questionable, due to not keeping up to date
with the dynamics of the business and business decisions.
Also the approach of estimating likelihood and consequence,
as part of risk management,
is not effective for emerging risks
\citep{eling_cyber_2021,
       ashby_emerging_2018}.
The emerging risks are the result of the
complexity created by the integration of IT systems,
business processes and organizations.
More generally stated,
the sequential approach
has proven inadequate in addressing the VUCA environment 
and the dynamic nature of cyber threats.
Implementation of (enterprise) risk management
will need a systems dynamics approach
\citep{eling_cyber_2021, 
       aven_enterprise_2019,
       bharathy_applying_2014,
       ashby_emerging_2018}.
The reality of an organization
having a nondeterministic behavior
\citep{boeing_visual_2016,
       lorenz_deterministic_1963,
       derbyshire_preparing_2014},
can not be compensated by deterministic checklists.
\citep{hoogervorst_foundations_2018,
       heylighen_complexity_2016}.

The complex nature of reality has recently (2023)
been somewhat accepted by the US National Institute of Standards and Technology (NIST),
by incorporating the Governance layer
into the their latest version of their
Cyber Security Framework (CSF) standard
\citep{national_institute_of_standards_and_technology_nist_2023}
and by 
US Department of Homeland Security,
via the Cyber-security and Infrastructure Security Agency (CISA),
requesting from all the governmental suppliers,
proof of their continuous risk assessment and applying relevant controls\footnote{via a Self Attestation Form: \url{https://www.cisa.gov/sites/default/files/2023-04/secure-software-self-attestation_common-form_508.pdf},
based on the NIST 800-218 on Secure Software Development Framework
\citep{souppaya_secure_2022}}.
This movement from delivering a checklist of the requirements
toward continuous evaluation
is not enough to protect our society from cyber incidents.
\\
The incorporation of continuous and automated checks,
does not mitigate the concept from complexity science
that we (as agents) are "intrinsically subjective and uncertain"
about our environment and future
\citep{heylighen_complexity_2016}
since nearly all things in our reality
are non-linear dynamic systems
\citep{boeing_visual_2016}.
The combination of us being uncertain,
and our reality being non-linear dynamac,
causes us to be in a state of continuous chaos,
since we cannot predict the future
\citep{lorenz_deterministic_1963}.
The continuous chaos applies to our objective reality
and our subjective reality
\citep{derbyshire_preparing_2014,
       botjes_attributes_2021}.
Objective chaos is created by the inter-connections
of systems, that quickly have exponential effects
(the butterfly effect),
\citep{lorenz_deterministic_1963}
and can be demonstrated
with a two-legged pendulum,
which is the simplest example
of two deterministic systems
creating one non-deterministic system
\citep{shinbrot_chaos_1992}. 
Subjective chaos is contributed to by the
differences in perspective\footnote{\url{https://plato.stanford.edu/entries/scientific-pluralism}} 
and 
perception\footnote{\url{https://plato.stanford.edu/entries/problem-of-many}}
\citep{schlick_general_1974,
       mcgilchrist_matter_2021}.
These two forces of chaos are
always present within organizations and in their context,
caused by the continuous need for innovation,
as the intersection of novelty, relevance and success
\citep{huber_innovation_2017} (fig 01).
\\
{\begin{tabularx}{\columnwidth}{C}
       \includegraphics[width=0.5\textwidth, fbox]{chaos}
       \\
       {\footnotesize Figure 01: Increasing subjective and objective chaos by
       innovation and interconnection}
       \\
       {\footnotesize
       \citep{derbyshire_preparing_2014, lorenz_deterministic_1963, shinbrot_chaos_1992}}
\end{tabularx}}
\\
\textbf{Antifragile organisations gain value from chaos}
\\
The goal of (all) organizations 
is to make sense of their internal and external context 
for all their stakeholders
\citep{op_t_land_applying_2008,
       osterwalder_business_2004, 
       van_steenbergen_survival_2023, 
       aven_enterprise_2019, 
       hutchins_iso_2018}.
This goal of organizations
is threatened by chaos.
This nondeterministic behavior of reality,
being the context of organizations or the interior of 
organization, have as effect that objective controls are fragile
to reality being it to the perception of the actor 
or the context in which it is being applied
\citep{dietz_enterprise_2020,
       van_gils_data_2023, 
       hoogervorst_foundations_2018, 
       aven_knowledge_2018}.
In 2012 the author Taleb
introduced the term antifragile,
which describes the anti-these of fragile behavior (fig 02)
\citep{taleb_antifragile_2012}.
Where a fragile system breaks when encountering stress,
an antifragile system gains in value by being exposed to stress.
Taleb states that an antifragile system gains from chaos.
Antifragile is the optimization in favor for the resilient behavior of a system.
to exploit unexpected events when they occur.
It is good to note that the academic field on antifragility is young 
\citep{botjes_attributes_2021}.
\\
    {\begin{tabularx}{\columnwidth}{C}
        \includegraphics[width=0.55\linewidth, fbox]{triad}
        \\
        {\footnotesize Figure 02: Antifragile as anti-these of fragile}
        \\
        {\footnotesize
        \citep{taleb_antifragile_2012, botjes_attributes_2021}}
    \end{tabularx}}
\\
To effectively navigate their complex landscape, 
organizations require a paradigm shift towards antifragility, 
a concept that emphasizes embracing and exploiting (unforeseen) 
disruptions to gain value and so to optimize resilience
\citep{eling_cyber_2021, 
       aven_enterprise_2019, 
       taleb_antifragile_2012, 
       aven_concept_2015,
       hutchins_iso_2018,
       martin-breen_bellagio_2011, 
       beukenkamp_securing_2016}.
Adapting and Adopting in reaction to a changing context,
organisations need to learn.
This is why key in becoming resilient and antifragile is to optimize for
learning and sense-making.
\\
\textbf{Cyber Resilience Maturity Models do not include organisational learning and antifragility}
\\
To improve an organization's cyber resilience, 
there are various Cyber Resilience Maturity Models (CRMM) 
available\footnote{Query: ("Review" 
    OR 
    "Systematic Review" 
    OR 
    "literature review"
) AND 
("Cyber Resilience" 
    OR 
    "Cyber-resilience" 
    OR 
    "cyber-resilient" 
    OR 
    "Cyber-Security Resilience"  
    OR 
    "Cyber Risk" 
    OR 
    "Cyber security" 
    OR 
    "Cybersecurity Resilience"
    OR 
    "Cyber security Resilience"
) AND 
("Maturity" 
    OR 
    "Maturity Models" 
    OR 
    "Frameworks"
)}
\citep{iyer_cyber_2024,
       marican_cyber_2023, 
       ciurea_trends_2023, 
       arai_cybersecurity_2022,  
       sepulveda_estay_systematic_2020, 
       carias_cyber_2020, 
       katsikas_analysis_2022, 
       chowdhury_key_2021, 
       le_capability_2017, 
       garba_explanatory_2020} 
e.g. 
the NIST Cybersecurity Framework
(CSF v2.0)\footnote{\url{https://www.nist.gov/cyberframework}}
\citep{national_institute_of_standards_and_technology_nist_2023},
%
the US Department of Defense
Cybersecurity Maturity Model Certification framework 
(CMMC)\footnote{\url{https://dodcio.defense.gov/CMMC/Model/}}
%
and the US Department of Energy
Cybersecurity Capability Maturity Model 
(C2M2)\footnote{\url{https://c2m2.doe.gov/}}.
%
There are also various CRMMs' not yet found
in our first scan of the available literature reviews on CRMMs:
the World Economic Forum
Cyber Resilience Framework (CRF)\footnote{\url{https://www.weforum.org/publications/the-cyber-resilience-index-advancing-organizational-cyber-resilience/}},
%
the World Bank 
Sectoral Cybersecurity Maturity Model (SCMM)
\citep{the_world_bank_sectoral_2023},
%
the PROGRESS Cyber-Capability Maturity Model (PROGRESS 
CCMM)
\citep{shaked_incorporating_2021},
%
the Cyber Defence Matrix,
%
The NIST guide on Developing Cyber-Resilient Systems:
A Systems Security Engineering Approach 
(NIST SP 800-160)
\citep{ross_developing_2021}.
%
A scan of the found CRMMs'
confirms that they are addressing resilience
from the point of view of reacting and dampening effects of events,
and do not address antifragile behavior.
Also none of these CRMMs' are incorporating concept from the domain
of the learning organization or means to address the subjective and
objective chaos.
\\
\hline
\end{xltabular}

