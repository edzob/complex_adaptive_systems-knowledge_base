The value of a conceptual model is not only to visualize 
\citep{van_der_waldt_constructing_2020}, 
it also has value for the creator of the model 
\citep{schoonderbeek_toward_2024}. 
"[a] system is not something presented to the observer, 
it is something to be recognized by him/her. 
Usually, this word does not refer to existing things 
in the real world but, rather, 
to a way of organizing our thoughts about the same." 
\citep{lukyanenko_system_2022, 
       skyttner_general_1996}.
A conceptual model 
aims to align the mental models 
of the reader and the writer,
with each other to increase the possibility of retrieving
the intention from the writer via the models,
being conceptual models or word
\citep{schoonderbeek_toward_2024, 
       proper_towards_2024}.
\cite{proper_towards_2024} 
defines a conceptual model as:
"A model of a domain, 
where the purpose of the model is dominated 
by the ambition to remain as-true-as-possible
to the original domain understanding.”, 
but the statement by 
\cite{guarino_philosophical_2019}
"conceptual models are not models of a given domain, 
but rather models of how we conceive of that domain"
fits the narrative of alignment of intent and perception
\citep{van_der_waldt_constructing_2020, 
       schoonderbeek_toward_2024, 
       hoppenbrouwers_fundamental_2005}
better.
Conceptual models hold value in the social 
\citep{van_der_waldt_constructing_2020}, 
as in the technical 
\citep{lukyanenko_system_2022} 
science.
\\
For this research proposal the conceptual model 
is used to visualize the mapping
of our research questions 
on the research approach of 
elaborated Action Design Research 
\citep{mullarkey_elaborated_2019}.
We recognize the existence of ontoUML and UFO,
\citep{guizzardi_types_2021}
and decided to apply the practical approach 
formulated  by 
\cite{van_der_waldt_constructing_2020} 
and the CESM\textsuperscript{+} checklist provided by 
\cite{lukyanenko_system_2022} 
due to the focus on 
alignment of the view on the process.
In the previous section we stated the research questions 
RQ1, RQ2 and RQ3. 
In the next section we are elaborating 
on the research design and methods.

\textbf{Research Questions}\\
The research questions to be approached with eADR are:
[RQ1]
Is there a Cyber Resilience Maturity Model (CRMM) 
that adequately addresses the exploitation of unforeseen events, 
as defined in the ISO 31000 on risk management?
[RQ2]
Does the (extended) Cyber Resilience Maturity Model (CRMM) 
offer guidance and value when 
applied by organizations?
[RQ3]
What are the key lessons learned 
from the application of the Cyber Resilience Maturity Model 
(CRMM) and how can these lessons be incorporated 
in to refined version of the CRMM?\\




\textbf{Research Methods}\\
The selection and refinement of an artifact (the real) 
will be done according to the 
elaborated Action Design Research (eADR) method 
[1. Diagnosis; 
2. Design; 
3. Implementation; 
4. Evolution]
\citep{mullarkey_elaborated_2019}.
eADR integrates the Action Design Research (ADR) research method 
[1. Problem formulation; 
2. Building, intervention and evaluation; 
3. Reflection and learning; 
4. Formalization of learning]
\citep{sein_action_2011, susman_assessment_1978},
and Design Science Research (DSR).
DSR started as a three-cycle 
[1. Relevance; 
2. Design; 
3.Rigor] approach
\citep{hevner_three_2007, hevner_design_2004}
and later defined four-cycles 
[1. Change \& impact; 
2. Relevance; 
3. Design; 
4. Rigor]
\citep{drechsler_four-cycle_2016,}.
eADR supports multiple entry points in the research cycle,
and supports emergent research 
in complex domains addressing "wicked" problems
\citep{mullarkey_elaborated_2019}.
This research adopts 
the school of Critical Realism (CR) that includes 
the objective of the real, 
as well as the subjective of the reality 
as dual ontological standpoints 
\citep{edwards_studying_2014, 
       uppstrom_designing_2017}. 
CR prescribes a mix-method approach, and
identifies the following research steps:
[1. Explication of events;
2. Explication of structure and context;
3. Retroduction;
4. Empirical corroboration;
5. Triangulation and multiple (mixed) methods.]
\citep{bygstad_identifying_2016,
       edwards_studying_2014,
       galliers_affordance_2017,
       mukumbang_contributions_2023}.
These CR steps fits in the eADR approach.
\\


\cite{mullarkey_elaborated_2019}
transformed the steps in ADR from
[1. Problem formulation; 
2. Building, intervention and evaluation; 
3. Reflection and learning; 
4. Formalization of learning] 
\cite{sein_action_2011} 
(fig 10) 
into the ADR intervention cycle steps
[1. Problem Formulation;
 2. Artifact Creation;
 3. Evaluation;
 4. Reflection;
 5. Formalisation of Learning.]
(fig 11). 

\\
{\begin{tabularx}{\textwidth}{CC} 
    \includegraphics[width=0.7\linewidth, fbox]{ADR}
    &
    \includegraphics[width=0.7\linewidth, fbox]{intervention ADR}
    \\
    {\footnotesize Figure 10: ADR} &    
    {\footnotesize Figure 11: ADR Intervention Cycle }
    \\
    {\footnotesize \citep{sein_action_2011}}
    &
    {\footnotesize \citep{mullarkey_elaborated_2019}}
    \\
\end{tabularx}}
\\

DSR cycles
[1. Diagnosis; 
2. Design; 
3. Implementation; 
4. Evolution]
%The retrospective analysis of the structures
%relevant to the research context fits 
%the Critical Realism (CR) approach,
%prescribes a mix-method approach, 
%and the following research steps:
%[1. Explication of events;
%2. Explication of structure and context;
%3. Retroduction;
%4. Empirical corroboration;
%5. Triangulation and multiple (mixed) methods.]
%\citep{bygstad_identifying_2016,
%       edwards_studying_2014,
%       galliers_affordance_2017,
%       mukumbang_contributions_2023,
%       uppstrom_designing_2017}.
%These CR steps fits in the eADR approach.


\\
{\begin{tabularx}{\textwidth}{C} 
    \includegraphics[width=0.3\linewidth, fbox]{intervention ADR}
    \\
    {\footnotesize Figure 11: ADR Intervention Cycle 
    within the eADR cycles}   
    \\    
    {\footnotesize \citep{mullarkey_elaborated_2019}}   
    \\
\end{tabularx}}
\\



and the CESM\textsuperscript{+} checklist provided by 
\cite{lukyanenko_system_2022} 


\textbf{Cyber Resilience Maturity Models Research Plan}
\\
The focus of our research is:
•  Identify the available
Maturity Models on the topic of Cyber Resilience
in the academic literature;
•  Create a criteria list
based on the collected theory
on risk management
and holistic socio-technical approach of Cyber Resilience
and Antifragility;
•  Filter the found Cyber Resilience Maturity Models (CRMMs')
with these criteria;
•  Create a criteria list
based on the existing critique
on the quality of maturity models and their function;
•  Validate the remaining CRMMs'
on these criteria;
•  Extend an existing CRMM where needed;
and
•  Validate the application of the CRMM in business context.
We acknowledge
that there are many Cyber Resilience Maturity Models (CRMMs')
available in the business context,
and that this amount
will grow fast in the coming years
due to the increase of legislation
in the EU (NIS2, RED, DORA, CRA, \ldots)
as in the US
and the increased impact of cyber incidents
on the continuity of organizations.
We also acknowledge that
there are CRMMs' in the academic literature,
and we have yet not found
any CRMM in the academic literature that addresses
antifragile behavior.
Our research will contribute
to the ongoing discourse on Cyber Resilience
and provide practical insights for organizations
seeking to enhance their resilience capabilities
in an increasingly complex and interconnected world.
\\
The selection and refinement of an artifact (the real)
will be done according to the
elaborated Action Design Research (eADR) approach
[1. Diagnosis;
2. Design;
3. Implementation;
4. Evolution]
\citep{mullarkey_elaborated_2019}.
eADR integrates the Action Design Research (ADR) approach
[1. Problem formulation;
2. Building, intervention and evaluation;
3. Reflection and learning;
4. Formalization of learning]
\citep{sein_action_2011, susman_assessment_1978},
and Design Science Research (DSR).
DSR started as a three-cycle
[1. Relevance;
2. Design;
3.Rigor] approach
\cite{hevner_three_2007, hevner_design_2004}
and later defined four-cycles
[1. Change \& impact;
2. Relevance;
3. Design;
4. Rigor]
\cite{drechsler_four-cycle_2016,}.
eADR supports multiple entry points in the research cycle,
and supports emergent research
in complex domains addressing "wicked" problems
\citep{mullarkey_elaborated_2019}.
This research adopts
the school of critical realism that includes
the objective of the real,
as well as the subjective of the reality
as dual ontological standpoints
\citep{edwards_studying_2014,
       uppstrom_designing_2017}.
\\

\\
The goal of the elaborated action research approach (eADR),
executed in more the one context,
is to validate how actionable the content of the CRMM is,
and if there is a cohesion between
determining the actions,
taking the actions and evaluating the improvement.
A complete validated CRMM is too much of work
to see as outcome of this research.
The prediction is that the result will be a refined CRMM
that includes exploiting unforeseen events,
and indicated as actionable by more the one context.

The upside of this is that an existing CRMM already
might have some contributors and users increasing
the possibility of application and replication.
When the need is there to create a CRMM from scratch
more time of this research will go into the first
application at an organization
and adoption by the
Cyber Resilience community.
The upside of this scenario is that there will be less
energy needed to convince people that their original CRMM
might be improved.

