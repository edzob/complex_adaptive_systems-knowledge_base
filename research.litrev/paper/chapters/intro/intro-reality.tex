%!TEX root = ../main.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Constant change}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Change is always around us according to
the Greek philosopher 
Heraclitus\footnote{\url{https://iep.utm.edu/heraclit}} 
(fl. c. 500 BC) 
\citep[p.tbd]{heyd_spoilers_2018}.
The change around us 
was and is also relevant to 
organisations\footnote{\url{https://scholar.google.com/scholar?q=change+fast+world}} 
\citep{mintzberg_rise_1994, 
mack_managing_2015, 
kok_leading_2018, 
bennett_what_2014, 
hutchins_iso_2018}.


For organisations it is
important to make sense 
of their internal and external context 
and adapt their value proposition 
\citep[p. 19, 50]{osterwalder_business_2004}
and their internal construction 
\citep[sec. 5.2]{janssen_enterprise_2015}
accordingly 
to stay relevant to 
all their stakeholders
(\cites[ch. 4]{hoogervorst_foundations_2018}
    [ch. 1]{mintzberg_rise_1994}
    [ch. 2]{op_t_land_enterprise_2009}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Technology supports decision making}
\label{sec:decission}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Organisations use technology 
to store data and  generate information 
to support actions in their operational activities
\citep[ch. 1]{dietz_enterprise_2020}.
See \ref{fig:semiotic-ladder}, figure \ref{fig:EA} 
and \ref{fig:osi-cloud-stack}.


\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\linewidth]{paper//images/semiotic-ladder.png}
    \caption{Semiotic Ladder from \cite{dietz_enterprise_2020}}
    \label{fig:semiotic-ladder}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\linewidth]{EA.png}
    \caption{Enterprise Architecture Layer model}
    \label{fig:EA}
\end{figure}


Technology supporting organisations in the storage and retrieval
of information
has evolved from clay tablets 
to complex computer systems
(\cites[ch. 1-2]{dietz_enterprise_2020}
[...]{schwarzkopf_virtual_2015}).
See fig \ref{fig:osi-cloud-stack}.

There are various meanings and definitions to 
adhere onto data, information and action
    (\cites[sec 2.2.4]{dietz_enterprise_2020}
           [...]{van_gils_data_2023}
           [fig. 1.1 \& 4.10]{hoogervorst_foundations_2018}
           [vol 1 - ... ]{mcgilchrist_matter_2021}).

When the word data is used 
as the materialisation of an observations, 
then at least the following viewpoint are possible;
• Data as reflection of our mutual synchronisation of our reality (Cognitive Neuroscience)
    (\cites[...]{downes_models_2009}
           [...]{hoffman_interface_2015}),
• Data as reflection by the language we speak (psycholinguistics),
\cites[..]{mykhailyuk_languages_2015}
      [..]{spivey_how_2012}
      [...]{louwerse_keeping_2021}
      [...]{lupyan_linguistically_2012}
      [...]{lleras_metaphors_2014},
• Data as reflection of the language used for interaction (...)
    (\cites[fig. 5]{hestenes_notes_2006}
           [...]{ogden_meaning_1923}
           [...]{stamper_semiotic_1991}),
• Data as reflection of our internal mental models (perception)
    (\cites[fig. 5]{hestenes_notes_2006}
           [...]{qin_predictability_2023}
           [...]{taleb_black_2007}
           [...]{johnson-laird_mental_2010}
           [...]{ciborra_labyrinths_2004})
• Data as reflection of social interactions (...)
    (\cites[...]{habermas_theory_1986}
           [...]{van_reijswoud_structure_1998}
           [...]{whalen_transaction_2011}).    

It is not evident to make a choice 
between the various viewpoints 
on the perception of reality 
and the influence of language and social interaction in this. 
That is why we state that the resilient behaviour 
of an organisation needs to include 
the social as the technological aspects since they both influence
data, information and action
\citep{lapalme_enterprise--environment_2014, 
       volkoff_critical_2013, 
       kotusev_can_2020}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Reality}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The notion that there are various ways to observe, 
make sense 
and respond to our reality is not new.
In the fresco School of Athens\footnote{\url{https://en.wikipedia.org/wiki/The_School_of_Athens}} 
by Raphael 
the philosophers 
Plato\footnote{\url{https://en.wikipedia.org/wiki/Plato}}\\ 
(428 BC - 348 BC) and 
Aristotle\footnote{\url{https://en.wikipedia.org/wiki/Aristotle}} 
(384 BC - 322 BC) 
are portrait as the objective and subjective way 
to observe reality (Ontology\footnote{\url{https://plato.stanford.edu/entries/logic-ontology}}) 
\citep[...]{masliakova_cultural_2021}.

There is a spectrum of flavours 
between the subjective and objective view 
of understanding of reality
(\cites[...]{scott_education_2013}
       [...]{groff_critical_2004}
       [...]{uppstrom_designing_2017}).
Critical realism (as ontology)
states that both sides of the spectrum are true and 
need to be respected 
\citep[...]{edwards_critical_2014}.
Critical Realism applies to both 
the social as to the technological
aspects of our reality 
\citep[...]{uppstrom_designing_2017}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Chaos}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

“Nearly all nontrivial real-world systems 
are nonlinear dynamical systems.” 
\citep[...]{boeing_visual_2016}. 
Nonlinear dynamical systems display chaotic behaviour
\citep{lorenz_deterministic_1963}. 
Chaos is 
defined as
“when the present determines the future, 
but the approximate present 
does not approximately determine the future” 
\citep{lorenz_deterministic_1963}.
Chaos, as a subset of unpredictability, 
coexists out of objective and subjective chaos 
\citep{derbyshire_preparing_2014}.

Reality is a construction of various forms of
double pendulums 
\citep{shinbrot_chaos_1992}
leading to the objective chaos. 
Subjective chaos is contributed to by the
differences in 
perspective\footnote{\url{https://plato.stanford.edu/entries/scientific-pluralism}} and 
perception\footnote{\url{https://plato.stanford.edu/entries/problem-of-many}}
(\cites[333]{schlick_general_1974}
       [...]{mcgilchrist_matter_2021})
and the need to innovate 
\citep{huber_innovation_2017}.
See figure \ref{fig:non-linair}.


\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{paper//images/non-linair.png}
    \caption{leading to a nonlinear dynamical systems}
    \label{fig:non-linair}
\end{figure}

The unpredictability of reality threatens the
continuity of organisations.